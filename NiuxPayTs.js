var NiuxPayTs = (function () {
    function NiuxPayTs() {
        this.sessionid = '';
        this.username = '';
        this.usernick = '';
        this.opid = 'web';
        this.mref = "";
        this.activeid = '2005001';
        this.tradeid = '1001';
        this.version = 'v2.0';
        this.niuxActNo = '';
        this.niuxAdvNo = '';
        this.bank = 'ccb';
        this.isPayQR = true;
        this.paytype = '';
        this.netpaytype = 'E';
        this.niupayway = '支付宝';
        this.payRatio = 100;
        this.checkPayOrderInter = null;
        this.payOrderCheckTimes = 0;
        this.checkLimitTimes = 60;
        this.payCheckCount = 0;
        this.bizno = 'yeyou';
        this.GAMELIST = [];
        this.gameList = [];
        this.PYS = "abcdefghijklmnopqrstuvwxyz";
        this.excludeGame = ["05"];
        this.includeGame = ["00", "06"];
        this.gamePlayRecently = false;
        this.serverPlayRecently = false;
        this.gamenameSel = '选择游戏';
        this.servernameSel = '';
        this.roleid = '';
        this.isNeedRole = false;
        this.paySvrUrl = 'http://paysvr.niu.xunlei.com:8090/';
        this.niuxWebsiteUrl = 'http://niu.xunlei.com/';
        this.gameUserInfo = 'http://websvr.niu.xunlei.com/userGameServerList.gameUserInfo';
        this.mPayUrl = 'https://agent-paycenter-ssl.xunlei.com/';
        this.priceTags = [10, 30, 50, 100, 200, 500, 1000, 5000, 10000];
        this.priceMy = 100;
        this.isUserDefinedMoney = false;
        this.serverGroupNum = 5;
        this.extparam = '';
        this.NOWTIME = new Date().getTime();
        this.records = 10;
        this.priceLimit = 150000;
        this.count = 1;
        this.counts = 10;
        this.NiuxpayPage = document.createElement('div');
        if (typeof arguments[0] != undefined) {
            var _config = arguments[0] || {};
            for (var obj in _config) {
                if (obj in this) {
                    this[obj] = _config[obj];
                }
            }
        }
        this.init();
    }
    NiuxPayTs.prototype._init = function () {
        if (typeof arguments[0] != undefined) {
            var _config = arguments[0] || {};
            for (var obj in _config) {
                if (obj in this) {
                    this[obj] = _config[obj];
                }
            }
        }
    };
    NiuxPayTs.prototype.init = function () {
        this.createStyle();
        this.createDom();
        this.initJqObj();
        this.initGames();
    };
    NiuxPayTs.prototype.createStyle = function () {
        var styleHtmlList = [];
        styleHtmlList.push('<style>');
        styleHtmlList.push("#NiuxPayTs,dd,dl,form,h1,h2,h3,h4,h5,h6,p{margin:0}#NiuxPayTs,input,ol,ul{margin:0;padding:0}#NiuxPayTs,button,input,select,textarea{font:12px/1.5 \u5FAE\u8F6F\u96C5\u9ED1, tahoma, arial, \u5B8B\u4F53, sans-serif;outline:0}ol,ul{list-style:none}a{text-decoration:none;outline:0 none}img{border:0}table{border-collapse:collapse;border-spacing:0}button{cursor:pointer}i{font-style:normal}.fix:after{visibility:hidden;display:block;font-size:0;content:\".\";clear:both;height:0}.hide{display:none}.fc_red{color:#f3730e}.pay_pop_wp{background:#fff;border:#eaeaea solid 1px;box-shadow:0 0 5px #1a2c49;color:#999;width:720px;position:relative}.pay_pop_wp01{width:600px}.pay_pop_wp02{width:500px}.pay_pop_hd{background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat;height:34px;position:relative}.pay_pop_hd h3{font-size:16px;font-weight:400;color:#fff;line-height:34px;width:120px;text-align:center}.pay_pop_clo{background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -186px -42px;position:absolute;top:14px;right:18px;width:12px;height:12px;text-indent:-999em}.pay_pop_cont{background:#fff;color:#666;position:relative;padding:0 20px 10px}.img_wp img{display:block;width:100%;height:100%}.lastdiv{margin-right:0!important}.price_txt{margin:16px 0 7px;font-size:14px}.no_mgt{margin-top:0!important}.price_num{color:#fc8321}.price_num em{font-size:24px}.tip_grey{font-size:14px;line-height:1.5;color:#969696}.tip_grey a{color:#969696}.tip_grey a:hover{text-decoration:underline}.tip_orange{font-size:12px;color:#fc8321}a.tip_orange:hover{text-decoration:underline}.btn_orange{display:inline-block;width:164px;height:50px;line-height:50px;color:#fff;font-size:14px;text-align:center;background:#fc8321;transition:.2s}.btn_orange:hover{background:#fe9525}.ico_suc48x48,.ico_warn34x34,.ico_warn48x48{display:inline-block;vertical-align:middle;margin-right:12px;width:48px;height:48px;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat}.ico_warn48x48{background-position:0 -42px}.ico_warn34x34{background-position:-118px -108px;width:34px;height:34px}.ico_suc48x48{background-position:-59px -42px}.mark_tui{position:absolute;right:0;top:0;width:59px;height:59px;line-height:59;background:url(http://act.niu.xunlei.com/niuxpayts/img/ico_spr.png) no-repeat -150px -200px;overflow:hidden}.overhid{overflow:hidden}.txtcent{text-align:center}.mlr30{margin-left:30px;margin-right:30px}.mt28{margin-top:28px!important}.mt40{margin-top:40px!important}.mb10{margin-bottom:10px!important}.mb20{margin-bottom:20px!important}.mb30{margin-bottom:30px!important}.ml70{margin-left:70px}.plr45{padding:0 45px}.pl45{padding-left:45px}.pl90{padding-left:90px!important}.pt20{padding-top:20px}.pb40{padding-bottom:40px}.pt65{padding-top:65px!important}.pt190{padding-top:190px}.pb190{padding-bottom:190px}.top9{top:9px!important}.clr_orange2{color:#fc8321}.bb{border-bottom:1px solid #f5f5f5}.bt{border-top:1px solid #f5f5f5}.font14{font-size:14px}.bank_alipay,.bank_cft,.ico_bank{display:block;width:120px;height:33px;background:url(http://act.niu.xunlei.com/niuxpayts/img/spr_bank.png) no-repeat}.bank_ccb{background-position:0 -250px}.bank_icbc{background-position:0 -600px}.bank_cmb{background-position:0 -400px}.bank_boc{background-position:0 -150px}.bank_abc{background-position:0 -50px}.bank_ceb{background-position:0 -300px}.bank_cib{background-position:0 -350px}.bank_spdb{background-position:0 0}.bank_pab{background-position:0 -500px}.bank_cmbc{background-position:0 -450px}.bank_bosh{background-position:0 -200px}.bank_bjrcb{background-position:0 -100px}.bank_gdb{background-position:0 -550px}.bank_nbcb{background-position:0 -650px}.bank_bocm{background-position:0 -1050px}.bank_other{background-position:0 -700px}.bank_alipay{width:124px;height:38px;background-position:-10px -803px}.bank_cft{width:124px;height:38px;background-position:-10px -853px}.cnmobile{width:124px;height:38px;background-position:-10px -903px}.unicom{width:124px;height:38px;background-position:-10px -953px}.telecom{width:124px;height:38px;background-position:-10px -1003px}.fm_group{position:relative;margin:8px 0 0;padding-left:90px;font-size:14px}.fm_group .label{position:absolute;left:0;top:3px;line-height:2;font-size:14px;width:80px;color:#969696;text-align:right}.fm_group .txt_acc{display:block;line-height:30px}.fm_group .inp_txt{display:inline-block;width:283px;height:22px;padding:2px 10px;margin-right:10px;border:1px solid #e6e6e6;vertical-align:top}.fm_group .inp_txt_min{width:88px;clear:both}.fm_group .inp_txt_mid{width:240px}.fm_group .inp_txt_mid01{width:145px}.fm_group .inp_txt:focus{outline:0}.fm_group .authcode{display:inline-block;width:90px;height:40px;margin-right:8px;vertical-align:middle}.fm_group .authcode img{display:block;width:100%;height:100%;border:none}.fm_group .tex_tipmsg{margin:2px 0 -8px 0;font-size:12px;color:#d02525}.fm_group_min{text-align:left}.tip_sticky{display:block;width:512px;height:28px;line-height:28px;background-color:#fff6ef;font-size:12px;color:#969696;text-align:center}.tip_sticky .clr{color:#fc8321}.tab_area{overflow:hidden}.tab_area .tips{text-align:left;padding-top:6px}.tab_area .yzm{display:inline-block;vertical-align:top;width:82px;height:34px}.tab_area .yzm img{width:82px;height:34px}.tab_area .txt_change{display:inline-block;vertical-align:top;line-height:34px;padding-left:10px;color:#f15849;text-decoration:underline}.choicebox_wp{position:relative;zoom:1;padding-top:2px;overflow:hidden}.choicebox_wp .choicebox{position:relative;float:left;margin:0 8px 4px 0;min-width:120px;height:33px;background:#fff;border:1px solid #dcdcdc;text-align:center;cursor:pointer}.choicebox_wp .overhid{width:92%;padding-top:2px}.choicebox_wp .choicebox_txt{float:none;width:303px;height:33px}.choicebox_wp .choicebox_txtmini{margin-right:5px;width:148px;min-width:148px;height:33px}.choicebox_wp .choicebox_txt p,.choicebox_wp .choicebox_txtmini p{line-height:33px;font-size:14px}.choicebox.on{z-index:2}.choicebox.on:before{content:'';position:absolute;top:-1px;left:-1px;right:-1px;bottom:-1px;border:2px solid #fc8321}.choicebox:hover{border-color:#fc8321;z-index:1}.choicebox img{display:block;margin:0 auto}.choicebox .tit{display:block;height:33px;line-height:33px;font-size:14px;color:#323232}.choicebox .tip{display:block;color:#969696}.mark_nian{position:absolute;left:-9px;top:5px;width:66px;height:26px;background:#fc8321;color:#fff;line-height:26px;text-align:center}.mark_nian:after{content:\"\";position:absolute;left:0;bottom:-7px;width:0;height:0;border-width:4px;border-style:solid;border-color:#d66f1c #d66f1c transparent transparent}.mark_privilege{position:absolute;right:0;top:0;padding:0 6px;height:21px;line-height:21px;color:#fff;background:#fc8321}.mark_selected{display:none;position:absolute;right:-1px;bottom:-1px;overflow:hidden;width:20px;height:20px;line-height:30;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -152px -42px}.choicebox.on .mark_selected{display:block}.choicebox input.num{background:#fff;padding:6px 6px 7px;margin:0 auto;width:84px;height:20px;line-height:20px;text-align:center;outline:0;font-size:14px;border:none;color:#323232;font-weight:400}.choicebox.on input.num{position:relative;z-index:10;background:0 0}.choicebox .tip_orange{position:absolute;left:-2%;top:90px;width:104%}.choicebox_large .choicebox{width:124px;height:38px}.tab_list{position:relative;height:46px;border-bottom:1px solid #e3e3e3;background:#fff;font-size:0;white-space:nowrap}.tab_list a{display:inline-block;vertical-align:middle;position:relative;padding:0 9px;height:46px;line-height:46px;color:#a8a8a8;font-size:14px;cursor:pointer;transition:.2s}.tab_list a:after{content:'';position:absolute;right:0;top:50%;margin-top:-5px;vertical-align:middle;width:1px;height:11px;background:#e3e3e3}.tab_list a:hover{color:#fc8321}.tab_list a.on{color:#fc8321;border-left-color:#f5f5f5;border-right-color:#f5f5f5;border-bottom-color:#fff}.tab_list a.on:before{content:'';position:absolute;left:1%;bottom:-2px;width:98%;height:3px;background:#f3730e;z-index:1}.selectbox_wp:after{display:block;content:\"\";clear:both;height:0;position:relative}.select_box{position:relative;float:left;vertical-align:middle;margin-right:8px;height:35px}.selected{position:relative;width:120px;height:33px;border:1px solid #e6e6e6;background:#fff}.selected a{display:block;padding:0 43px 0 10px;height:100%;line-height:33px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;color:#4e4e4e}.select_box .option_open_btn{position:absolute;right:0;top:0;width:33px;height:33px;cursor:pointer}.select_box .option_open_btn i{position:absolute;left:50%;top:50%;margin:-3px 0 0 -6px;width:0;height:0;overflow:hidden;border-width:6px 6px 0;border-style:solid dashed;border-color:#a1a1a1 #fff}.select_box .option_open_btn .option_close{border-width:0 6px 6px}.select_box .option_open_btn:hover i{border-color:#fc8321 #fff!important}.option_wp{position:absolute;top:35px;left:0;width:512px;border:1px solid #e6e6e6;background:#fff;z-index:10;box-shadow:0 3px 5px rgba(0,0,0,.2)}.option_wp .option_txt{display:block;padding:0 13px;width:142px;height:28px;border-bottom:1px solid #e5e5e5;color:#969696;line-height:28px;transition:.2s}.option_wp .option_txt:hover{color:#fc8321}.select_box_2 .option_wp{left:-130px}.option_wp_2{position:absolute;top:35px;left:0;width:120px;max-height:370px;overflow:auto;border:1px solid #e6e6e6;background:#fff;box-shadow:0 3px 5px rgba(0,0,0,.2)}.option_wp_2 a{display:block;height:37px;line-height:37px;padding:0 10px;color:#6b6b6b;white-space:normal;overflow:hidden;text-overflow:ellipsis;transition:.2s}.option_wp_2 a:hover{background:#f3730e;color:#fff}.option_list{position:relative;padding:14px 0 10px;height:170px;overflow:auto}.option_group{position:relative;padding:0 12px 0 31px}.option_group ul{overflow:hidden}.option_group li{float:left;height:30px;line-height:30px;width:113px;margin-bottom:5px}.option_group li a{display:inline-block;max-width:99px;padding:0 6px;font-size:12px;color:#6b6b6b;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;transition:.2s}.option_group li a:hover{background:#f3730e;color:#fff}.option_group .keyword{position:absolute;top:5px;left:14px;color:#fc8321}.option_group_min{padding:0 12px}.option_group_min li{width:117px}.option_group_min li a{max-width:105px}.option_group_3col{padding:0 12px}.option_group_3col li{width:157px}.option_group_3col li a{max-width:140px}.search_mod{width:298px;margin:-14px auto 14px;padding:13px 57px;background:#e3e3e3}.ipt_search{position:relative;height:27px;background:#fff}.ipt_search input{display:block;border:none;height:17px;width:238px;padding:5px 30px}.ipt_search input:focus{outline:0}.ipt_search .ico_search{position:absolute;top:4px;left:6px;width:18px;height:18px;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -155px -69px}.ipt_wp{position:relative;margin-bottom:7px;line-height:30px}.ipt_wp .label{display:inline-block;margin-right:14px;height:30px;vertical-align:middle}.tick_box{display:inline-block;vertical-align:middle;margin-right:4px;width:16px;height:16px;border:1px solid #cbcbcb;border-radius:1px}.tick_box.on{background:url(http://act.niu.xunlei.com/niuxpayts/img/ico_spr.png) no-repeat -250px -125px}.tip_bg{padding:12px 20px;background:#fff6ef;text-align:center;font-size:14px}.paywaycont_wp{position:relative;padding:4px 0 0;background:#fff;clear:both;color:#969696;text-align:center;width:520px}.privilege_wp{padding-left:70px}.privilege_wp.fix_padl{padding-left:56px}.paywaycont_wp .btn_orange{margin-bottom:4px}.paywaycont_wp .lnk_addtic{color:#7e7e7e}.paywaycont_wp .lnk_addtic .ico_add{display:inline-block;vertical-align:-3px;width:15px;height:15px;margin-right:3px;background:url(http://act.niu.xunlei.com/niuxpayts/img/ico_spr.png) no-repeat -300px -100px}.paywaycont_wp .pay_btn_wp{padding:8px 22px 0 0}.qrcodepay_wp{position:relative;font-size:14px;line-height:1.5;text-align:left}.qrcodepay_wp .pic_qrcode{position:relative;width:160px;height:160px;margin-left:100px;background:#fff;border:1px solid #e5e5e5}.qrcodepay_wp .pic_qrcode img{display:block;width:100%}.qrcodepay_wp .pd{padding:20px 0 0 60px}.qrcodepay_wp .tip_payway{text-align:left;position:absolute;left:310px;bottom:60px}.qrcodepay_wp .pic_payway{margin-bottom:6px}.qrcodepay_wp .pic_payway .ico_alipay,.qrcodepay_wp .pic_payway .ico_wechat{display:inline-block;width:30px;height:30px;vertical-align:top;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -36px -110px}.qrcodepay_wp .pic_payway .ico_wechat{background-position:-74px -110px}.qrcodepay_wp .ico_pointto{position:absolute;left:-40px;top:10px;width:29px;height:13px;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat 0 -118px}.pic_qrcode .qrcode_off{position:absolute;left:0;top:0;width:100%;height:100%}.pic_qrcode .qrcode_off .mask{position:absolute;left:0;top:0;width:100%;height:100%;background:#fff;-moz-opacity:.9;filter:alpha(opacity=90);opacity:.9}.pic_qrcode .qrcode_off .txt{position:relative;z-index:1;margin:68px auto 0;width:122px;height:28px;line-height:28px;text-align:center;color:#fff;background:#666;background:rgba(0,0,0,.6);border-radius:14px;cursor:default}.pay_rezult{padding:0 30px 30px}.pay_pop_wp .maintxt{margin-top:30px;font-size:16px;text-align:center}.pay_pop_wp .txt_rows li{line-height:30px;font-size:14px}.pay_pop_wp .p_btn_wp{width:100%;text-align:center}.pay_pop_wp .p_btn_wp .btn{text-indent:0;display:inline-block;vertical-align:top;margin:0 4px;padding:0 20px;min-width:80px;width:auto;height:40px;line-height:40px;color:#fff;font-size:12px;background:#ffb174;transition:.2s;letter-spacing:0}.pay_pop_wp .p_btn_wp .btn_orange{background:#fc8321;border-color:#fc8321;color:#fefefe;box-shadow:none;font-size:14px}.pay_pop_wp .p_btn_wp .btn_orange:hover{background:#fe9525}.pay_pop_wp .p_btn_wp .btn_blue{background:#4591ef;border-color:#4591ef;color:#fefefe;box-shadow:none;font-size:14px}.pay_pop_wp .p_btn_wp .btn_blue:hover{background:#5c9ff1}.pay_pop_wp .tips_min{padding-left:100px;font-size:14px}@-webkit-keyframes jump{0%,100%,12%,18%,24%,30%,6%{-webkit-transform:translateY(0)}15%,27%,3%{-webkit-transform:translateY(-5%)}21%,9%{-webkit-transform:translateY(5%)}}@keyframes jump{0%,100%,12%,18%,24%,30%,6%{transform:translateY(0)}15%,27%,3%{transform:translateY(-5%)}21%,9%{transform:translateY(5%)}}.zindex10{position:relative;z-index:10}.pay_pop_wp{position:fixed;top:50%;left:50%;margin-top:-254px;margin-left:-361px}.game_option_list{display:none}.game_option_list.on{display:block!important}#NiuxPayTs .fenqu_option_wp .option_group ul{display:none}#NiuxPayTs .fenqu_option_wp .option_group ul.on{display:block}#NiuxPayPop2{margin-left:-250px}#NiuxPayPop3,#NiuxPayPop4,#NiuxPayPop5{margin-left:-300px}#NiuxPayTs a:hover{text-decoration:none}");
        styleHtmlList.push('</style>');
        $('head').append(styleHtmlList.join(''));
    };
    NiuxPayTs.prototype.createDom = function () {
        this.NiuxpayPage = document.createElement('div');
        this.NiuxpayPage.setAttribute('id', 'NiuxPayTs');
        this.NiuxpayPage.style.cssText = 'display:none;';
        var PayPopDom = "<div class=\"niuxpay_mask\" style=\"display:none;position:fixed;width:100%;height:100%;top:0;left:0;background:#000;opacity:.6;filter:alpha(opacity=60);z-index:10;\"></div><div class=pay_pop_wp id=NiuxPayPop1 style=\"z-index:11\"><div class=pay_pop_hd><h3>\u6D3B\u52A8\u5145\u503C</h3><a href=\"#\" title=\"\u5173\u95ED\" class=pay_pop_clo>\u5173\u95ED</a></div><div class=pay_pop_cont><div class=pay_form><div class=fm_group><span class=label>\u5145\u503C\u8D26\u53F7\uFF1A</span><span class=txt_acc>yangfei@163.com</span></div><div class=\"fm_group zindex10\"><span class=label>\u5145\u503C\u4FE1\u606F\uFF1A</span><div class=selectbox_wp><div class=select_box><div class=selected data-tab=1><a href=\"javascript:;\" id=xlGameSel></a><span class=option_open_btn><i class=option_open></i><i class=option_close style=display:none></i></span></div><div class=\"option_wp game_option_wp\" style=display:none><div class=\"tab_list tab_list_1\"><a href=\"javascript:;\" class=on data-gamestab=0>\u6700\u8FD1\u73A9\u8FC7\u7684</a><a href=\"javascript:;\" data-gamestab=1>ABCDE</a><a href=\"javascript:;\" data-gamestab=2>FGHIJ</a><a href=\"javascript:;\" data-gamestab=3>KLMNO</a><a href=\"javascript:;\" data-gamestab=4>PQRST</a><a href=\"javascript:;\" data-gamestab=5>UVWXYZ</a></div></div></div><div class=\"select_box select_box_2\"><div class=selected data-tab=2><a href=\"javascript:;\" id=xlGServerSel>\u9009\u62E9\u533A\u670D</a><span class=option_open_btn><i class=option_open></i><i class=option_close style=display:none></i></span></div><div class=\"option_wp fenqu_option_wp\" style=display:none><div class=\"tab_list tab_list_2\"></div><div class=option_list><div class=search_mod><div class=ipt_search><i class=ico_search></i><input type=text id=NiuxPayQFs></div></div><div class=\"option_group option_group_min option_group_3col xl_fenqus_options\"></div></div></div></div><div class=select_box><div class=selected data-tab=3><a href=\"javascript:;\" id=xlGRoleSel>\u9009\u62E9\u89D2\u8272</a><span class=option_open_btn><i class=option_open></i><i class=option_close style=display:none></i></span></div><div class=\"option_wp option_wp_2 role_option_wp\" style=display:none></div></div></div></div><div class=fm_group><span class=label>\u5145\u503C\u91D1\u989D\uFF1A</span><div class=\"choicebox_wp col5\"><div class=choicebox_moneys></div><div class=\"choicebox choicebox-oth\"><span class=tit style=display:none>\u5176\u5B83</span><input class=num type=text placeholder=\"\u5176\u4ED6\u91D1\u989D\"><i class=mark_selected>\u9009\u4E2D</i></div></div></div><div class=fm_group><span class=label>\u652F\u4ED8\u65B9\u5F0F\uFF1A</span><div class=\"choicebox_wp col5\" id=xlPayways><div class=\"choicebox on\"><span class=tit>\u626B\u7801\u652F\u4ED8</span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox><span class=tit>\u5E73\u53F0\u652F\u4ED8</span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox><span class=tit>\u7F51\u94F6\u652F\u4ED8</span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox><span class=tit>\u96F7\u70B9\u652F\u4ED8</span><i class=mark_selected>\u9009\u4E2D</i></div></div><div class=paywaycont_wp id=xlPayway><div class=tab_area style=display:block><div class=qrcodepay_wp><div class=pic_qrcode><img src=http://act.niu.xunlei.com/niuxpayts/images/code.png alt=\"\u652F\u4ED8\u4E8C\u7EF4\u7801\"><div class=img_loading></div><div class=qrcode_off><div class=mask></div><div class=txt>\u8BF7\u9009\u62E9\u5145\u503C\u4FE1\u606F</div></div></div><div class=tip_payway><div class=pic_payway><span class=ico_alipay></span><span class=ico_wechat></span></div><p class=tip_grey>\u8BF7\u4F7F\u7528\u5FAE\u4FE1\u6216\u652F\u4ED8\u5B9D\u626B\u7801\u652F\u4ED8</p><p class=tip_grey>\u540C\u610F\u5E76\u63A5\u53D7<a href=http://i.xunlei.com/tos.shtml target=_blank class=link>\u300A\u8FC5\u96F7\u652F\u4ED8\u670D\u52A1\u534F\u8BAE\u300B</a></p><span class=ico_pointto></span></div><p class=\"tip_grey pd\">\u5B9E\u4ED8\u91D1\u989D<strong class=fc_red><font class=payIn>100</font>\u5143</strong>\uFF0C\u60A8\u5C06\u83B7\u5F97<strong class=fc_red><font class=payOut>10000</font>\u5143\u5B9D</strong></p></div></div><div class=tab_area style=display:none><div class=\"choicebox_wp choicebox_large\"><div class=\"choicebox on\" title=\"\u652F\u4ED8\u5B9D\" data-paytype=E><span class=\"ico_bank bank_alipay\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u8D22\u4ED8\u901A\" data-paytype=E2><span class=\"ico_bank bank_cft\"></span><i class=mark_selected>\u9009\u4E2D</i></div></div><div class=\"pay_btn_wp mt20\"><a href=\"javascript:;\" class=btn_orange>\u7ACB\u5373\u5145\u503C</a><p class=tip_grey>\u5B9E\u4ED8\u91D1\u989D<strong class=fc_red><font class=payIn>100</font>\u5143</strong>\uFF0C\u60A8\u5C06\u83B7\u5F97<strong class=fc_red><font class=payOut>10000</font>\u5143\u5B9D</strong></p></div></div><div class=tab_area style=display:none><div class=\"choicebox_wp choicebox_mid\"><div class=\"choicebox on\" title=\"\u4E2D\u56FD\u5EFA\u8BBE\u94F6\u884C\" data-bank=CCB><span class=\"ico_bank bank_ccb\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4E2D\u56FD\u5DE5\u5546\u94F6\u884C\" data-bank=ICBC><span class=\"ico_bank bank_icbc\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u62DB\u5546\u94F6\u884C\" data-bank=CMB><span class=\"ico_bank bank_cmb\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4E2D\u56FD\u94F6\u884C\" data-bank=BOCB2C><span class=\"ico_bank bank_boc\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4E2D\u56FD\u519C\u4E1A\u94F6\u884C\" data-bank=ABC><span class=\"ico_bank bank_abc\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4E2D\u56FD\u5149\u5927\u94F6\u884C\" data-bank=CEBBANK><span class=\"ico_bank bank_ceb\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u5174\u4E1A\u94F6\u884C\" data-bank=CIB><span class=\"ico_bank bank_cib\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4E0A\u6D77\u6D66\u4E1C\u53D1\u5C55\u94F6\u884C\" data-bank=SPDB><span class=\"ico_bank bank_spdb\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u5E73\u5B89\u94F6\u884C\" data-bank=SPABANK><span class=\"ico_bank bank_pab\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4E2D\u56FD\u6C11\u751F\u94F6\u884C\" data-bank=CMBC><span class=\"ico_bank bank_cmbc\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4E0A\u6D77\u94F6\u884C\" data-bank=SHBANK><span class=\"ico_bank bank_bosh\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u5317\u4EAC\u519C\u6751\u5546\u4E1A\u94F6\u884C\" data-bank=BJRCB><span class=\"ico_bank bank_bjrcb\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u5E7F\u4E1C\u53D1\u5C55\u94F6\u884C\" data-bank=GDB><span class=\"ico_bank bank_gdb\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u5B81\u6CE2\u94F6\u884C\" data-bank=NBBANK><span class=\"ico_bank bank_nbcb\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=choicebox title=\"\u4EA4\u901A\u94F6\u884C\" data-bank=COMM><span class=\"ico_bank bank_bocm\"></span><i class=mark_selected>\u9009\u4E2D</i></div><div class=\"choicebox choicebox_mini\" title=\"\u5176\u4ED6\u94F6\u884C\" data-bank=\"\"><span class=\"ico_bank bank_other\"></span><i class=mark_selected>\u9009\u4E2D</i></div></div><div class=\"pay_btn_wp mt20\"><a href=\"javascript:;\" class=btn_orange>\u7ACB\u5373\u5145\u503C</a><p class=tip_grey>\u5B9E\u4ED8\u91D1\u989D<strong class=fc_red><font class=payIn>100</font>\u5143</strong>\uFF0C\u60A8\u5C06\u83B7\u5F97<strong class=fc_red><font class=payOut>10000</font>\u5143\u5B9D</strong></p></div></div><div class=tab_area style=display:none><div class=tip_sticky><p>\u53EF\u5148\u4F7F\u7528\u624B\u673A\u5145\u503C\u5361\u3001\u8FC5\u96F7/\u5929\u5F18\u4E00\u5361\u901A\u7B49\u5148\u5145<a href=https://pay.xunlei.com/ld.html class=clr target=_blank>\u96F7\u70B9</a></p></div><div class=\"fm_group fm_group_min\"><span class=label>\u8D26\u6237\u4F59\u989D\uFF1A</span><p class=tips><font id=NiuxLD></font><span class=clr_gray>\u96F7\u70B9</span>\uFF081\u96F7\u70B9=1\u5143\uFF09</p></div><div class=\"fm_group fm_group_min\"><span class=label>\u652F\u4ED8\u5BC6\u7801\uFF1A</span><input type=password class=\"inp_txt inp_txt_mid\" id=NiuxPayPs><p class=tex_tipmsg style=\"visibility:hidden;\">\u652F\u4ED8\u5BC6\u7801\u4E0D\u6B63\u786E</p></div><div class=\"fm_group fm_group_min\" id=NiuxPayVc><span class=label>\u9A8C\u8BC1\u7801\uFF1A</span><input type=password class=\"inp_txt inp_txt_mid01\"><img class=yzm src=http://act.niu.xunlei.com/niuxpayts/images/yzm.jpg><a href=\"#\" class=txt_change>\u6362\u4E00\u4E2A</a></div><div class=\"pay_btn_wp mt30\"><a href=\"javascript:;\" class=btn_orange>\u7ACB\u5373\u5145\u503C</a><p class=tip_grey>\u5B9E\u4ED8\u91D1\u989D<strong class=fc_red><font class=payIn>100</font>\u5143</strong>\uFF0C\u60A8\u5C06\u83B7\u5F97<strong class=fc_red><font class=payOut>10000</font>\u5143\u5B9D</strong></p></div></div></div></div></div></div></div><div class=\"pay_pop_wp pay_pop_wp02\" id=NiuxPayPop2 style=\"display:none;z-index:12;\"><div class=pay_pop_hd><h3>\u63D0\u793A</h3><a href=\"#\" title=\"\u5173\u95ED\" class=pay_pop_clo>\u5173\u95ED</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class=maintxt>\u8BA2\u5355\u67E5\u8BE2\u4E2D\u00B7\u00B7\u00B7\u00B7\u00B7\u00B7</p></div></div></div></div><div class=\"pay_pop_wp pay_pop_wp01\" id=NiuxPayPop3 style=\"display:none;z-index:13;\"><div class=pay_pop_hd><h3>\u63D0\u793A</h3><a href=\"#\" title=\"\u5173\u95ED\" class=pay_pop_clo>\u5173\u95ED</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class=\"maintxt fc_red\"><i class=ico_suc48x48></i>\u606D\u559C\u60A8\u5145\u503C\u6210\u529F</p><div class=tips_min><ol></ol></div></div></div><div class=p_btn_wp><a href=\"javascript:;\" class=\"btn btn_orange\">\u5173\u95ED</a></div></div></div><div class=\"pay_pop_wp pay_pop_wp01\" id=NiuxPayPop4 style=\"display:none;z-index:14;\"><div class=pay_pop_hd><h3>\u63D0\u793A</h3><a href=\"#\" title=\"\u5173\u95ED\" class=pay_pop_clo>\u5173\u95ED</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class=maintxt><i class=ico_warn34x34></i>\u8BF7\u5728\u65B0\u6253\u5F00\u7684\u9875\u9762\u5B8C\u6210\u652F\u4ED8</p><ul><li>\u5B8C\u6210\u652F\u4ED8\u524D\u8BF7\u4E0D\u8981\u5173\u95ED\u6B64\u7A97\u53E3<li>\u652F\u4ED8\u5931\u8D25\u65F6\uFF0C\u53EF\u62E8\u62530755-61860414\uFF0C\u8FC5\u96F7\u5BA2\u670D\u7AED\u8BDA\u4E3A\u60A8\u670D\u52A1\uFF01</ul></div></div><div class=p_btn_wp><a href=\"javascript:;\" class=\"btn btn_orange\" data-btn=200>\u5DF2\u5B8C\u6210\u652F\u4ED8</a><a href=\"javascript:;\" class=btn data-btn=1>\u652F\u4ED8\u5931\u8D25\uFF0C\u91CD\u8BD5</a></div></div></div><div class=\"pay_pop_wp pay_pop_wp01\" id=NiuxPayPop5 style=\"display:none;z-index:15;\"><div class=pay_pop_hd><h3>\u63D0\u793A</h3><a href=\"#\" title=\"\u5173\u95ED\" class=pay_pop_clo>\u5173\u95ED</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class=\"maintxt fc_red\"><i class=ico_warn48x48></i>\u8BF7\u5728\u65B0\u6253\u5F00\u7684\u9875\u9762\u5B8C\u6210\u652F\u4ED8</p>\u53EF\u80FD\u539F\u56E0\uFF1A<ol><li>1\u3001\u60A8\u7684\u652F\u4ED8\u8FC7\u7A0B\u6CA1\u6709\u5B8C\u6210\u3002<li>2\u3001\u94F6\u884C/\u8D26\u6237\u5DF2\u6263\u6B3E\uFF0C\u4F46\u6CA1\u6709\u63D0\u793A\u6210\u529F\uFF1F\u7F51\u7EDC\u4F20\u8F93\u95EE\u9898\u53EF\u80FD\u9020\u6210\u5EF6\u65F6\uFF0C\u8BF7\u4E0D\u8981\u62C5\u5FC3\uFF0C\u6211\u4EEC\u6676\u5757\u4F1A\u652F\u4ED8\u6E20\u9053\u5BF9\u8D26\u540E\uFF0C\u6062\u590D\u6B64\u670D\u52A1\uFF0C\u4E3A\u60A8\u5F00\u901A\u670D\u52A1\u3002<li>3\u3001\u94F6\u884C/\u8D26\u6237\u6CA1\u6709\u6263\u6B3E\uFF1F\u5EFA\u8BAE\u60A8\u91CD\u65B0\u652F\u4ED8\u4E00\u6B21\u8BD5\u8BD5\u770B\u5982\u679C\u4ECD\u65E0\u6CD5\u6210\u529F\uFF0C\u8BF7\u8054\u7CFB\u76F8\u5173\u94F6\u884C/\u901A\u4FE1\u8FD0\u8425\u5BA2\u670D<li>4\u3001\u5982\u679C\u90A3\u8FD8\u4E0D\u80FD\u5E2E\u5230\u60A8\uFF0C\u8BF7\u62E8\u62530755-61860414\uFF0C\u8FC5\u96F7\u5BA2\u670D\u7AED\u8BDA\u4E3A\u60A8\u670D\u52A1\u3002<li>\u652F\u4ED8\u5931\u8D25\u65F6\uFF0C\u53EF\u62E8\u62530755-61860414\uFF0C\u8FC5\u96F7\u5BA2\u670D\u7AED\u8BDA\u4E3A\u60A8\u670D\u52A1\uFF01</ol></div></div><div class=p_btn_wp><a href=\"javascript:;\" class=\"btn btn_orange\" data-btn=200>\u91CD\u65B0\u67E5\u8BE2</a><a href=\"javascript:;\" class=btn data-btn=1>\u53D6\u6D88</a></div></div></div> ";
        $(this.NiuxpayPage).html(PayPopDom);
        document.body.appendChild(this.NiuxpayPage);
    };
    NiuxPayTs.prototype.initJqObj = function () {
        var _this = this;
        _this.$username = $('#NiuxPayTs .txt_acc');
        _this.$payDom = $('#NiuxPayTs');
        _this.$gamesel = $('#xlGameSel');
        _this.$payMoneys = _this.$payDom.find('.choicebox_moneys');
        _this.$gamelist = _this.$payDom.find('.game_option_wp');
        _this.$gamelistTab = _this.$payDom.find('.tab_list_1');
        _this.$gameqflist = _this.$payDom.find('.fenqu_option_wp');
        _this.$serverSel = $('#xlGServerSel');
        _this.$roleSel = $('#xlGRoleSel');
        _this.$roleList = _this.$payDom.find('.role_option_wp');
        _this.$payways = $('#xlPayways .choicebox');
        _this.$payway = $('#xlPayway .choicebox');
        _this.$pricePi = _this.$payDom.find('.choicebox-oth');
        _this.$payqrOff = _this.$payDom.find('.qrcode_off');
        _this.$payQRImg = _this.$payDom.find('.pic_qrcode img');
        _this.$NiuxPayMask = _this.$payDom.find('.niuxpay_mask');
        _this.$optionOpen = _this.$payDom.find('.option_open');
        _this.$optionClose = _this.$payDom.find('.option_close');
        if (!_this.isUserDefinedMoney) {
            _this.$pricePi.hide();
        }
        _this.$qufuSearch = $('#NiuxPayQFs');
        _this.$payIn = $('#xlPayway .payIn');
        _this.$payOut = $('#xlPayway .payOut');
        _this.$NiuxPayPop1 = $('#NiuxPayPop1');
        _this.$NiuxPayPop2 = $('#NiuxPayPop2');
        _this.$mainTip = _this.$NiuxPayPop2.find('.maintxt');
        _this.$NiuxPayPop3 = $('#NiuxPayPop3');
        _this.$NiuxPayPop4 = $('#NiuxPayPop4');
        _this.$NiuxPayPop5 = $('#NiuxPayPop5');
        _this.usernick = $.cookie('usernick') || $.cookie('usrname') || $.cookie('usernewno') || '没有登陆';
        _this.username = $.cookie('usrname') || $.cookie('usernewno') || '';
        _this.userid = $.cookie('userid') || '';
        _this.usernewno = $.cookie("usernewno") || "";
        _this.sessionid = $.cookie('sessionid') || '';
        _this.mref = $.cookie('mref') || '';
        _this.niuxActNo = $.cookie('niuxActNo') || '';
        _this.niuxAdvNo = $.cookie('niuxAdvNo') || '';
        _this.unitPrice = _this.priceMy * 100;
        _this.$username.text(this.usernick);
        _this.$gamesel.text(this.gamenameSel);
        $('#NiuxPayTs .select_box .selected').eq(2).hide();
        _this.ieV = parseInt($.browser.version);
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (elt) {
                var len = this.length >>> 0;
                var from = Number(arguments[1]) || 0;
                from = (from < 0)
                    ? Math.ceil(from)
                    : Math.floor(from);
                if (from < 0)
                    from += len;
                for (; from < len; from++) {
                    if (from in this &&
                        this[from] === elt)
                        return from;
                }
                return -1;
            };
        }
    };
    NiuxPayTs.prototype.initGames = function () {
        var _this = this;
        if (typeof (GAMES) == 'undefined' || GAMES == null) {
            $.getScript('http://pay.niu.xunlei.com/js/config/baseConfig.js', function () {
                _this.initGames();
            });
            return;
        }
        _this.GAMES = GAMES || null;
        var payMoneysTam = "";
        var priceIdx = _this.priceTags.indexOf(_this.priceMy);
        for (var i = 0; i < _this.priceTags.length; i++) {
            payMoneysTam += "<div class=\"choicebox" + (i == priceIdx ? " on" : "") + "\" data-money=\"" + _this.priceTags[i] + "\"><span class=\"tit\">" + _this.priceTags[i] + "\u5143</span> <i class=\"mark_selected\">\u9009\u4E2D</i></div>";
        }
        _this.$payMoneys.html(payMoneysTam);
        _this.$payIn.text(_this.priceMy);
        _this.$payOut.text(_this.priceMy * _this.payRatio);
        if (_this.ieV < 10) {
            _this.$pricePi.find('input').val('其他金额');
        }
        if (_this.gameList.length > 0) {
            var _GAMES = [];
            for (var g = 0; g < _this.gameList.length; g++) {
                for (var gg in GAMES) {
                    if (_this.gameList[g] == GAMES[gg].gameid) {
                        _GAMES.push(GAMES[gg]);
                    }
                }
            }
            _this.gamesListUi(_GAMES);
        }
        else {
            _this.GAMESListUi(GAMES);
        }
        this.bindEvent();
    };
    NiuxPayTs.prototype.gamesListUi = function (gl) {
        var _this = this;
        _this.$gamelistTab.hide();
        var optionlist = "<div class=\"option_list game_option_list on\" style=\"height:auto;\"><div class=\"option_group\"><ul>";
        for (var j = 0; j < gl.length; j++) {
            optionlist += "<li><a href=\"#\" data-gamename=\"" + gl[j].gamepath + "\">" + gl[j].name + "</a></li>";
        }
        optionlist += "</ul></div></div>";
        _this.$gamelist.html(optionlist);
    };
    NiuxPayTs.prototype.GAMESListUi = function (gl) {
        var _this = this;
        _this.$gamelistTab.show();
        for (var i = 0; i < _this.PYS.length + 1; i++) {
            _this.GAMELIST[i] = new Array();
        }
        var py;
        for (var g in gl) {
            if (gl[g].simpleName == undefined) {
                continue;
            }
            if (gl[g].status == 2) {
                continue;
            }
            if (_this.excludeGame.indexOf(gl[g].gameid.substr(0, 2)) > -1) {
                continue;
            }
            if (_this.includeGame.indexOf(gl[g].gameid.substr(0, 2)) < 0) {
                continue;
            }
            py = gl[g].simpleName.toString().split("")[0].toLowerCase();
            if (_this.PYS.indexOf(py) > -1) {
                _this.GAMELIST[_this.PYS.indexOf(py)].push(gl[g]);
            }
        }
        var optionlist = "<div class=\"option_list game_option_list on\"></div><div class=\"option_list game_option_list\">";
        for (var j = 0; j < this.GAMELIST.length - 1; j++) {
            if (j == 5 || j == 10 || j == 15 || j == 20) {
                optionlist += "</div><div class=\"option_list game_option_list\">";
            }
            if (this.GAMELIST[j].length != 0) {
                optionlist += "<div class=\"option_group\"><span class=\"keyword\">" + this.PYS[j] + "-</span><ul>";
                for (var k = 0; k < this.GAMELIST[j].length; k++) {
                    optionlist += "<li><a href=\"#\" data-gamename=\"" + this.GAMELIST[j][k].gamepath + "\">" + this.GAMELIST[j][k].name + "</a></li>";
                }
                optionlist += "</ul></div>";
            }
        }
        optionlist += "</div>";
        _this.$gamelist.append(optionlist);
        _this.gamesMyListUI();
    };
    NiuxPayTs.prototype.gamesMyListUI = function () {
        var _this = this;
        $.ajax({
            url: _this.gameUserInfo,
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'rtnName',
            data: {
                records: _this.records,
                sessionid: _this.sessionid,
                username: _this.username
            },
            success: function (res) {
                if (res.rtn == 0) {
                    _this.gamesMy = res.data.data;
                    var optionlist2 = "<div class=\"option_group\"><ul>";
                    for (var i = 0; i < res.data.data.length; i++) {
                        optionlist2 += "<li><a href=\"#\" data-gamename=\"" + res.data.data[i].gamename + "\">" + res.data.data[i].name + "</a></li>";
                    }
                    optionlist2 += "</ul></div>";
                    _this.$gamelist.find('.game_option_list').eq(0).html(optionlist2);
                }
            }
        });
    };
    NiuxPayTs.prototype.selGame = function (gameno) {
        var _this = this;
        _this.gameid = _this.GAMES[gameno].gameid;
        _this.isNeedRole = false;
        if (_this.GAMES[gameno].formType == 'usr') {
            _this.isNeedRole = true;
        }
        _this.servernameSel = '';
        _this.serverId = '';
        _this.$serverSel.text('选择区服');
        _this.$roleSel.text('选择角色');
        _this.roleMy = '';
        _this.$gamelist.toggle();
        _this.$gameqflist.toggle();
        $.getScript(_this.niuxWebsiteUrl + 'gamedata/fenqu/' + _this.gameid + '.json', function () {
            _this.gamefenqus = gamefenqus;
            if (_this.gamefenqus.length < 1)
                return;
            if (_this.gamefenqus[0].fenQuNum > _this.gamefenqus[_this.gamefenqus.length - 1].fenQuNum) {
                _this.gamefenqus = _this.gamefenqus.reverse();
            }
            _this.gamefenqusUI(_this.gamefenqus);
        });
        $.ajax({
            url: _this.gameUserInfo,
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'rtnName',
            data: {
                gameId: _this.gameid,
                records: _this.records,
                sessionid: _this.sessionid,
                username: _this.username
            },
            success: function (res) {
                if (res.rtn == 0) {
                    _this.gamesMy = res.data.data;
                    var optionlist2 = "";
                    for (var i = 0; i < _this.gamesMy.length; i++) {
                        optionlist2 += "<li><a href=\"#\" data-serverid=\"" + _this.gamesMy[i].serverid + "\">" + _this.gamesMy[i].fenqunickname + "</a></li>";
                    }
                    _this.$gameqflist.find('.xl_fenqus_options ul').eq(0).html(optionlist2);
                }
            }
        });
    };
    NiuxPayTs.prototype.selServer = function (serverId) {
        var _this = this;
        _this.$gameqflist.toggle();
        _this.$roleSel.text('选择角色');
        _this.roleMy = '';
        var rtnName = 'getGameRole' + (new Date()).getTime() + Math.floor(Math.random() * 1000);
        $.getScript(_this.paySvrUrl + 'gamepaycenter/getRole.role?gameid=' + _this.gameid + '&serverid=' + _this.serverId + '&username=' + _this.username + '&rtnName=' + rtnName, function () {
            _this.rtnName = window[rtnName];
            if (_this.rtnName.code == '00') {
                var rolesHTML = "";
                for (var m in _this.rtnName.roleMap) {
                    rolesHTML += "<a href=\"#\" data-roleid=\"" + m + "\">" + decodeURIComponent(_this.rtnName.roleMap[m]) + "</a>";
                }
                _this.$roleList.html(rolesHTML);
                $('#NiuxPayTs .select_box .selected').eq(2).show();
            }
            else {
                _this.$roleList.html('');
                _this.roleMy = '0';
                _this.$roleSel.text('无角色');
                if (_this.isPayQR) {
                    _this.makeQrcode();
                }
            }
        });
    };
    NiuxPayTs.prototype.gamefenqusUI = function (gf) {
        var fenquUITam;
        var _this = this;
        var fqlistTam = "<a href=\"javascript:;\" class=\"on\" data-qufu=\"0\">\u6700\u8FD1\u73A9\u8FC7\u7684</a>";
        var fenquTemplate = "<ul class=\"on\"></ul>";
        if (gf.length >= 100) {
            var fqC = 0;
            var fqL = [];
            var fqM = void 0;
            if (gf.length > 500) {
                fqM = 100;
            }
            else {
                fqM = Math.floor(gf.length / _this.serverGroupNum);
            }
            var fqI = 0;
            for (var i in gf) {
                if (!gf[i]) {
                    continue;
                }
                if (!fqL[fqC]) {
                    fqL[fqC] = [];
                }
                fqL[fqC].push(gf[i]);
                fqI++;
                if (fqI > fqM && fqL.length < _this.serverGroupNum) {
                    fqC += 1;
                    fqI = 0;
                }
            }
            for (var j = 0; j < fqL.length; j++) {
                fqlistTam += "<a href=\"javascript:;\"  data-qufu=\"" + Number(j + 1) + "\">" + fqL[j][0].fenQuNum + "-" + fqL[j][fqL[j].length - 1].fenQuNum + "</a>";
                fenquTemplate += "<ul>";
                for (var k = 0; k < fqL[j].length; k++) {
                    fenquTemplate += "<li><a href=\"#\" data-serverid=\"" + fqL[j][k].serverId + "\">" + fqL[j][k].fenQuName + "</a></li>";
                }
                fenquTemplate += "</ul>";
            }
        }
        else {
            fqlistTam += "<a href=\"javascript:;\"  data-qufu=\"1\">" + gf[0].fenQuNum + "-" + gf[gf.length - 1].fenQuNum + "</a>";
            fenquTemplate += "<ul>";
            for (var k = 0; k < gf.length; k++) {
                fenquTemplate += "<li><a href=\"#\" data-serverid=\"" + gf[k].serverId + "\">" + gf[k].fenQuName + "</a></li>";
            }
            fenquTemplate += "</ul>";
        }
        _this.$gameqflist.find('.tab_list_2').html(fqlistTam);
        fenquTemplate += "<ul></ul>";
        _this.$gameqflist.find('.xl_fenqus_options').html(fenquTemplate);
    };
    NiuxPayTs.prototype.getPayData = function () {
        var _this = this;
        var payObj = {
            tradeid: _this.tradeid,
            userid: _this.userid,
            version: _this.version,
            sessionid: _this.sessionid,
            opid: _this.opid,
            activeid: _this.activeid,
            bizno: _this.bizno,
            num: _this.priceMy,
            rmb: _this.unitPrice,
            mref: _this.mref,
            referfrom: _this.niuxAdvNo
        };
        return payObj;
    };
    NiuxPayTs.prototype.checkPayInfo = function () {
        var _this = this;
        if (_this.sessionid == undefined || _this.sessionid == '') {
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            _this.$mainTip.text('请先登陆');
            return false;
        }
        if (_this.gameid == undefined || _this.gameid == '') {
            if (_this.isPayQR) {
                return false;
            }
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            _this.$mainTip.text('请选择游戏');
            return false;
        }
        if (_this.serverId == '') {
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            _this.$mainTip.text('请选择区服');
            return false;
        }
        if (_this.isNeedRole) {
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            if (_this.roleMy == '0') {
                _this.$mainTip.text('暂无角色');
                return false;
            }
            _this.$mainTip.text('请选择角色');
            return false;
        }
        return true;
    };
    NiuxPayTs.prototype.makeQrcode = function () {
        var _this = this;
        if (!_this.checkPayInfo()) {
            return;
        }
        var url = _this.mPayUrl + "vippay/qrcode";
        _this.originalOrderid = _this.makeOriginalOrderid();
        var ext2 = 'userId=' + _this.userid + '&userName=' + _this.username + '&gameId=' + _this.gameid + '&gameName=' + _this.gamenameSel + '&unitPrice=' + _this.unitPrice + '&serverId=' + _this.serverId + '&servername=' + _this.servernameSel + '&roleId=' + this.roleid + '&roleName=' + _this.roleMy + '&originalOrderid=' + _this.originalOrderid + '&bankno=' + _this.bank + '&niuxActNo=' + _this.niuxActNo + '&extparam=' + _this.extparam + '&niuxAdvNo=' + _this.niuxAdvNo;
        var ext3 = '{"mAid":"101"}';
        var params = _this.getPayData();
        params = $.extend(params, {
            ext2: ext2,
            ext3: ext3
        });
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            data: params,
            success: function (res) {
                if (res.ret == 200) {
                    var qrcodeImg = new Image();
                    qrcodeImg.onload = function () {
                        _this.$payQRImg.attr('src', this.src);
                        _this.$payqrOff.hide();
                    };
                    qrcodeImg.src = res.data["qrcode"];
                    _this.setCheckOrderInter(3000);
                }
                else {
                    _this.$payqrOff.show();
                    _this.$NiuxPayPop1.hide();
                    _this.$NiuxPayPop2.show();
                    _this.$mainTip.text(res.msg);
                }
            }
        });
    };
    NiuxPayTs.prototype.setCheckOrderInter = function (interTime) {
        var _this = this;
        clearInterval(_this.checkPayOrderInter);
        _this.checkPayOrderInter = setInterval(function () {
            _this.checkOrderStatus();
            _this.payOrderCheckTimes++;
            if (_this.payOrderCheckTimes >= _this.checkLimitTimes) {
                _this.clearCheckOrderAction();
            }
        }, interTime ? interTime : 5000);
    };
    NiuxPayTs.prototype.checkOrderStatus = function () {
        var _this = this;
        if (_this.payOrderCheckTimes % 5 == 0) {
            var interTime = 3000 + (_this.payOrderCheckTimes * 500);
            _this.setCheckOrderInter(interTime);
        }
        else {
            var queryUrl = _this.paySvrUrl + "gamepaycenter/pay?action=queryOriginOrder&rtnName=chkOrderRtn&originalOrderid=" + _this.originalOrderid + '&rd=' + Math.random();
            $.getScript(queryUrl, function (rs) {
                var code = parseInt(chkOrderRtn.code);
                if (code == 0) {
                    _this.clearCheckOrderAction();
                    _this.orderId = chkOrderRtn.orderid;
                    _this.niupayway = '扫码支付';
                    _this.paySuccess();
                }
                else if (code == 2) {
                }
                else if (code == 1) {
                    if (_this.payCheckCount >= 2) {
                        _this.clearCheckOrderAction();
                    }
                    _this.payCheckCount++;
                }
                else {
                    _this.clearCheckOrderAction();
                }
            });
        }
    };
    NiuxPayTs.prototype.paySuccess = function () {
        var _this = this;
        _this.$NiuxPayPop1.hide();
        _this.$NiuxPayPop2.hide();
        _this.$NiuxPayPop4.hide();
        _this.$NiuxPayPop5.hide();
        _this.$NiuxPayPop3.show();
        _this.username = $.cookie('usrname');
        var tipTam = "<li>\u5145\u503C\u6E38\u620F\uFF1A" + _this.gamenameSel + "</li><li>\u5B9E\u4ED8\u91D1\u989D\uFF1A" + Number(_this.priceMy * _this.payRatio) + "\u5143\u5B9D/" + _this.priceMy + "\u5143";
        tipTam += "<li>\u5145\u503C\u5E10\u53F7\uFF1A" + _this.username + "</li><li>\u5145\u503C\u533A\u670D\uFF1A" + _this.servernameSel + "</li><li>\u652F\u4ED8\u65B9\u5F0F\uFF1A" + _this.niupayway + "</li>";
        _this.$NiuxPayPop3.find('ol').html(tipTam);
        _this.getPayOkTipsHandler();
    };
    NiuxPayTs.prototype.getPayOkTipsHandler = function () {
    };
    NiuxPayTs.prototype.showPayFail = function () {
        var _this = this;
        _this.$NiuxPayPop1.hide();
        _this.$NiuxPayPop2.hide();
        _this.$NiuxPayPop3.hide();
        _this.$NiuxPayPop4.hide();
        _this.$NiuxPayPop5.show();
        _this.getPayFailTipsHandler();
    };
    NiuxPayTs.prototype.getPayFailTipsHandler = function () {
    };
    NiuxPayTs.prototype.clearCheckOrderAction = function () {
        var _this = this;
        clearInterval(_this.checkPayOrderInter);
        _this.payOrderCheckTimes = 0;
    };
    NiuxPayTs.prototype.makeOriginalOrderid = function () {
        var _this = this;
        var userNewNo = _this.usernewno;
        userNewNo = (userNewNo && userNewNo.length <= 10) ? userNewNo : _this.getRandomString(10);
        var originalOrderid = Math.floor(_this.NOWTIME / 1000) + userNewNo + _this.getRandomString(5);
        while (originalOrderid.length < 25) {
            originalOrderid = originalOrderid + _this.getRandomString(1);
        }
        return originalOrderid;
    };
    NiuxPayTs.prototype.getRandomString = function (len) {
        var _len = len || 25;
        var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        var maxPos = $chars.length;
        var pwd = '';
        for (var i = 0; i < _len; i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    };
    NiuxPayTs.prototype.netpayGo = function () {
        var _this = this;
        if (!_this.checkPayInfo()) {
            return;
        }
        var url = _this.mPayUrl + 'vippay/buy';
        _this.originalOrderid = _this.makeOriginalOrderid();
        var ext2 = 'userId=' + _this.userid + '&userName=' + _this.username + '&gameId=' + _this.gameid + '&gameName=' + _this.gamenameSel + '&unitPrice=' + _this.unitPrice + '&serverId=' + _this.serverId + '&servername=' + _this.servernameSel + '&roleId=' + this.roleid + '&roleName=' + _this.roleMy + '&goodstimes=1&niuxActNo=' + _this.niuxActNo + '&extparam=' + _this.extparam + '&niuxAdvNo=' + _this.niuxAdvNo;
        var _ext3 = {
            name: _this.gamenameSel,
            des: _this.gamenameSel + '|' + _this.servernameSel + '|' + _this.unitPrice,
            mAid: 101,
            mLogin: 1
        };
        var ext3 = encodeURIComponent(JSON.stringify(_ext3));
        var params = _this.getPayData();
        params = $.extend(params, {
            ext2: ext2,
            ext3: ext3,
            aidFrom: '',
            paytype: _this.paytype,
            bankNo: _this.bank,
            fgUrl: window.location.href
        });
        if (_this.paytype == 'A1') {
            var _other1 = '';
            var verify_code = '';
            _other1 = hex_md5($.trim($('#NiuxPayPs').val()));
            verify_code = $.trim($('#NiuxPayVc input').val());
            params.activeid = '2005002';
            params = $.extend(params, {
                other1: _other1,
                VERIFY_CODE: verify_code
            });
        }
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            data: params,
            success: function (res) {
                if (res.ret == 200) {
                    if (res.data.code == 200) {
                        _this.orderId = res.data.orderId;
                        _this.$NiuxPayPop1.hide();
                        _this.$NiuxPayPop4.show();
                        $('.tex_tipmsg').css('visibility', 'hidden');
                        window.open(res.data.url);
                    }
                    else if (res.data.code == 201) {
                        $('#NiuxPayPs').val('');
                        $('#NiuxPayVc input').val('');
                        _this.paySuccess();
                    }
                    else {
                        _this.checkVerCode();
                        $('.tex_tipmsg').text(res.msg).css('visibility', 'visible');
                    }
                }
                else if (res.ret == -1) {
                    if (_this.paytype != 'A1') {
                        _this.$NiuxPayPop1.hide();
                        _this.$NiuxPayPop2.show();
                        _this.$mainTip.text(res.msg);
                        return;
                    }
                    $('.tex_tipmsg').text(res.msg).css('visibility', 'visible');
                    _this.checkVerCode();
                }
            }
        });
    };
    NiuxPayTs.prototype.checkVerCode = function () {
        var _this = this;
        var timeStamp = new Date();
        var verfCodeUrl = 'https://captcha-ssl.xunlei.com/image?t=MDA&cachetime=' + timeStamp;
        $('#NiuxPayVc img').attr('src', verfCodeUrl);
    };
    NiuxPayTs.prototype.checkNetpayStatus = function () {
        var _this = this;
        _this.$NiuxPayPop1.hide();
        _this.$NiuxPayPop2.show();
        _this.$mainTip.text('订单查询中······');
        var url = _this.mPayUrl + 'vippay/issucc';
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            data: {
                version: _this.version,
                orderId: _this.orderId,
                opid: _this.opid,
                userid: _this.userid,
                sessionid: _this.sessionid
            },
            success: function (res) {
                if (res.ret == 200) {
                    if (res.data.fpaysucc == 1) {
                        _this.paySuccess();
                    }
                    else {
                        _this.showPayFail();
                    }
                }
            }
        });
    };
    NiuxPayTs.prototype.checkLeiDian = function () {
        var _this = this;
        var url = _this.mPayUrl + 'minfo/ldQuery';
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            success: function (res) {
                if (res.ret == 200) {
                    $('#NiuxLD').text(res.data);
                }
            }
        });
    };
    NiuxPayTs.prototype.show = function () {
        this.$payDom.show();
        this.$NiuxPayMask.show();
    };
    NiuxPayTs.prototype.hide = function () {
        this.$payDom.hide();
        this.$NiuxPayMask.hide();
    };
    NiuxPayTs.prototype.bindEvent = function () {
        var _this = this;
        _this.show();
        _this.$NiuxPayPop1.find('.pay_pop_clo').bind('click', function (e) {
            e.preventDefault();
            _this.clearCheckOrderAction();
            _this.hide();
        });
        _this.$NiuxPayPop2.find('.pay_pop_clo').bind('click', function (e) {
            e.preventDefault();
            _this.$NiuxPayPop2.hide();
            _this.$NiuxPayPop1.show();
        });
        $('#NiuxPayTs .select_box .selected').bind('click', function (e) {
            e.stopPropagation();
            var t = $(e.currentTarget).data('tab');
            _this.$tab = t;
            if (_this.$tab == 1) {
                _this.$gameqflist.hide();
                _this.$roleList.hide();
            }
            else if (_this.$tab == 2) {
                if (_this.gameid == undefined || _this.gameid == '')
                    return;
                _this.$gamelist.hide();
                _this.$roleList.hide();
            }
            else {
                _this.$gamelist.hide();
                _this.$gameqflist.hide();
            }
            $(this).parent('.select_box').find('.option_wp').toggle();
            $(this).find('.option_open').toggle();
            $(this).find('.option_close').toggle();
            for (var tt = 0; tt < 3; tt++) {
                if (tt == t - 1)
                    continue;
                _this.$optionClose.eq(tt).hide();
                _this.$optionOpen.eq(tt).show();
            }
        });
        _this.$gamelist.bind('click', function (e) {
            e.preventDefault();
            var $e = $(e.target);
            if ($e.data('gamestab') != undefined) {
                var idx = $e.data('gamestab');
                $e.addClass('on').siblings().removeClass('on');
                _this.$gamelist.find('.game_option_list').eq(idx).addClass('on').siblings().removeClass('on');
            }
            if ($e.data('gamename') != undefined) {
                _this.gameid = $e.data('gamename');
                _this.gamenameSel = $e.text();
                _this.$gamesel.text(_this.gamenameSel);
                _this.selGame(_this.gameid);
                _this.$gamelist.parent().find('.option_open').toggle();
                _this.$gamelist.parent().find('.option_close').toggle();
                _this.$gameqflist.parent().find('.option_open').toggle();
                _this.$gameqflist.parent().find('.option_close').toggle();
            }
        });
        _this.$gameqflist.bind('click', function (e) {
            e.preventDefault();
            var $e = $(e.target);
            if ($e.data('qufu') != undefined) {
                var idx = $e.data('qufu');
                $e.addClass('on').siblings().removeClass('on');
                _this.$gameqflist.find('.xl_fenqus_options ul').eq(idx).addClass('on').siblings().removeClass('on');
            }
            if ($e.data('serverid') != undefined) {
                _this.serverId = $e.data('serverid');
                _this.servernameSel = $e.text();
                _this.$serverSel.text(_this.servernameSel);
                _this.selServer(_this.serverId);
                _this.$gameqflist.parent().find('.option_open').toggle();
                _this.$gameqflist.parent().find('.option_close').toggle();
            }
        });
        _this.$qufuSearch.bind('textchange', function (e) {
            var _serverName = e.target.value;
            var _servers = [];
            for (var i = 0; i < _this.gamefenqus.length; i++) {
                if (_this.gamefenqus[i].fenQuName.indexOf(_serverName) > -1) {
                    _servers.push(_this.gamefenqus[i]);
                }
            }
            var _serversHTML = "";
            for (var j = 0; j < _servers.length; j++) {
                _serversHTML += "<li><a href=\"#\" data-serverid=\"" + _servers[j].serverId + "\">" + _servers[j].fenQuName + "</a></li>";
            }
            _this.$gameqflist.find('.xl_fenqus_options ul').eq(-1).html(_serversHTML).addClass('on').siblings().removeClass('on');
        });
        _this.$roleList.bind('click', function (e) {
            e.preventDefault();
            var $e = $(e.target);
            if ($e.data('roleid') != undefined) {
                var id = $e.data('roleid');
                _this.roleid = id;
                _this.$roleSel.text($e.text());
                _this.roleMy = $e.text();
                _this.$roleList.toggle();
                _this.isNeedRole = false;
                if (_this.isPayQR) {
                    _this.makeQrcode();
                }
            }
        });
        _this.$payways.bind('click', function () {
            var tab_index = $(this).index();
            if (tab_index == 3) {
                _this.isPayQR = false;
                _this.paytype = 'A1';
                _this.clearCheckOrderAction();
                _this.niupayway = '雷点支付';
                _this.checkVerCode();
                _this.checkLeiDian();
            }
            else if (tab_index == 1) {
                _this.isPayQR = false;
                _this.paytype = _this.netpaytype;
                _this.clearCheckOrderAction();
            }
            else if (tab_index == 2) {
                _this.isPayQR = false;
                _this.paytype = 'B';
                _this.clearCheckOrderAction();
            }
            else {
                _this.isPayQR = true;
                _this.makeQrcode();
            }
            $(this).addClass('on').siblings().removeClass('on');
            $('#xlPayway .tab_area').eq(tab_index).show().siblings().hide();
        });
        _this.$payway.bind('click', function (e) {
            var $e = $(e.currentTarget);
            $(this).addClass('on').siblings().removeClass('on');
            if ($e.data('bank') != undefined) {
                _this.bank = $e.data('bank');
                _this.niupayway = $e.attr('title');
            }
            if ($e.data('paytype') != undefined) {
                _this.netpaytype = $e.data('paytype');
                _this.paytype = _this.netpaytype;
                _this.niupayway = $e.attr('title');
            }
        });
        _this.$NiuxPayPop1.find('.btn_orange').bind('click', function (e) {
            e.preventDefault();
            _this.netpayGo();
        });
        _this.$payMoneys.find('.choicebox').bind('click', function () {
            _this.count += 1;
            if (_this.count > _this.counts) {
                _this.count = 1;
                _this.$NiuxPayPop1.hide();
                _this.$NiuxPayPop2.show();
                _this.$mainTip.text('订单请求过于频繁，请稍后再试');
                return;
            }
            $(this).addClass('on').siblings().removeClass('on');
            _this.$pricePi.removeClass('on');
            _this.priceMy = $(this).data('money');
            _this.unitPrice = _this.priceMy * 100;
            _this.$payIn.text(_this.priceMy);
            _this.$payOut.text(_this.priceMy * _this.payRatio);
            _this.$payqrOff.show();
            _this.$pricePi.find('input').val('');
            if (_this.ieV < 10) {
                _this.$pricePi.find('input').val('其他金额');
            }
            if (_this.isPayQR) {
                _this.makeQrcode();
            }
        });
        _this.$pricePi.find('input').bind('textchange', function (e) {
            var _val = $(this).val().replace(/\D/ig, "");
            _val = parseInt(_val) || 1;
            if (_val > _this.priceLimit) {
                _val = _this.priceLimit - 1;
            }
            $(this).val(_val);
            _this.priceMy = _val || 1;
            _this.unitPrice = _this.priceMy * 100;
            _this.$payIn.text(_this.priceMy);
            _this.$payOut.text(_this.priceMy * _this.payRatio);
            _this.$payqrOff.show();
        }).bind('blur', function (e) {
            if (e.target.value != '') {
                $(this).val(_this.priceMy + '元');
            }
            if (e.target.value == '' && _this.ieV < 10) {
                $(this).val('其他金额');
            }
            if (_this.isPayQR && _this.priceMy != 0) {
                _this.makeQrcode();
            }
        }).bind('focus', function (e) {
            _this.$payMoneys.find('.choicebox').removeClass('on');
            _this.$pricePi.addClass('on');
            $(this).val('');
            _this.priceMy = 0;
            _this.unitPrice = _this.priceMy * 100;
            _this.$payIn.text(_this.priceMy);
            _this.$payOut.text(_this.priceMy * _this.payRatio);
        });
        _this.$NiuxPayPop3.find('a').bind('click', function (e) {
            e.preventDefault();
            _this.$NiuxPayPop1.show();
            _this.$NiuxPayPop3.hide();
        });
        _this.$NiuxPayPop4.find('a').bind('click', function (e) {
            e.preventDefault();
            var $e = $(e.target), btnCode = $e.data('btn');
            if ($e.data('btn') != undefined) {
                if (btnCode == 200) {
                    _this.checkNetpayStatus();
                }
                else {
                    _this.$NiuxPayPop1.show();
                    _this.$NiuxPayPop4.hide();
                }
            }
        });
        _this.$NiuxPayPop5.find('a').bind('click', function (e) {
            e.preventDefault();
            var $e = $(e.target), btnCode = $e.data('btn');
            if ($e.data('btn') != undefined) {
                if (btnCode == 200) {
                    _this.checkNetpayStatus();
                }
                else {
                    _this.$NiuxPayPop1.show();
                    _this.$NiuxPayPop5.hide();
                }
            }
        });
        $('#NiuxPayVc a').bind('click', function (e) {
            e.preventDefault();
            _this.checkVerCode();
        });
    };
    return NiuxPayTs;
}());
//# sourceMappingURL=NiuxPayTs.js.map