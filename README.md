# 牛X页游活动通用支付组件

## 开发环境

1. [typescript](https://www.w3cschool.cn/typescript/typescript-tutorial.html)
2. [nodejs](https://nodejs.org/zh-cn/)
3. [sublime typescript](https://github.com/Microsoft/TypeScript-Sublime-Plugin)
4. [jquery.splendid.textchange](https://github.com/spicyj/jquery-splendid-textchange)

利用 `typescript` 编译 `es3` 的 `js` 脚本，以兼容低版本 `IE`，默认为 `IE8` 为开发标准。 使用 `nodejs` 为 `typescript` 编译环境，编辑器为 `sublime` ，附相应的 `ts` 插件。

在本目录运行
```
npm install  or yarn
```
即可。仅仅安装 `jquery` 和 `jquery-cookie` 的声明文件。

`tsconfig.json` 为 `tsc`的编译配置文件。

用 `sublime` 编辑 `ts` 文件，`ctrl+b` 即可编译。

`jquery.splendid.textchange` 为兼容 `IE8` 的 `input` 实时事件的 `jq` 插件。


## 使用方式

### 开始
依赖  `jQuery`, `jQuery.cookie`, `jquery-splendid-textchange`。

游戏数据加载链接为 `http://pay.niu.xunlei.com/js/config/baseConfig.js`。

线上外网链接使用 `http://static.webgame.kanimg.com/act/niuxpayts/js/NiuxPayTs.min.js`  (已包含jquery-splendid-textchange)

### 初始化

#### 普通项目

```
var NiuxPayObj;
openPay(){
    var config = {
        priceTags: [10,50,100,200,500,1000,5000,10000,20000], // 金额选项，可为空
        priceMy: 100, // 默认金额， 可为空
        niuxActNo: 'bwzxxf', // 当前活动编号，默认取 cookie
        gameList:[], // 默认可选的游戏列表，可为空
        isUserDefinedMoney: true, // 是否可自定义金额，可为空
        getPayOkTipsHandler: function(){
            console.log('支付成功');
        },  // 支付成功回调
        getPayFailTipsHandler: function(){
            console.log(this.orderId+'支付失败');
        } // 失败回调
    };
    // 避免对象多次初始化
    if(NiuxPayObj instanceof NiuxPayTs){
        NiuxPayObj.show();
    }else{
        NiuxPayObj = new NiuxPayTs(config);
    }
}

```

#### 活动项目

```
...
if(G_ACT.NiuxPayObj instanceof NiuxPayTs){
    G_ACT.NiuxPayObj.show();
}else{
    G_ACT.NiuxPayObj = new NiuxPayTs(config);
}
```


`opid`, `mref`, `tradeid`, `paySvrUrl`, `niuxWebsiteUrl` 等等对象开头定义的全部参数都可自行初始化配置，以及 `_init` 重新配置。






