/// <reference path="node_modules/@types/jquery/index.d.ts" />
/// <reference path="node_modules/@types/jquery.cookie/index.d.ts" />

// 依赖 jquery.1.8.2, jq.cookie, jquery.splendid.textchange(兼容IE的input事件)
// NiuxPayTs.min文件已包含 jquery.splendid.textchange[https://github.com/spicyj/jquery-splendid-textchange] 的压缩文件


declare var GAMES: any;
declare var PAY_DJQ_SWITCH: any;
declare var gamefenqus: any[];
declare var chkOrderRtn: any;
declare var hex_md5: Function;

/**
 * 通用牛X支付浮层
 * lixingguang@xunlei.com
 * @class NiuxPayTs
 */
class NiuxPayTs {
    // 用户信息
    userid;
    usernewno;
    sessionid: string = '';
    username: string = '';
    usernick: string = '';

    // 活动信息
    opid = 'web';
    rtnName;
    mref= "";//支付渠道
    activeid = '2005001';
    tradeid = '1001';
    version = 'v2.0';
    unitPrice:number;
    niuxActNo = '';  // 活动编号
    niuxAdvNo = ''; // 支付渠道
    bank = 'ccb'; // 银行代号
    isPayQR = true;  // 默认扫码支付
    paytype = '';  // 支付方式代号E,E1,B,A1
    netpaytype = 'E';
    niupayway = '支付宝';  //支付方式的名称
    orderId; // 查询的订单号
    payRatio: number = 100; // 充值比率
    originalOrderid;  //生成的订单号
    checkPayOrderInter = null;  //支付订单回查定时器
    payOrderCheckTimes = 0; //回查订单次数
    checkLimitTimes = 60;  //回查订单限制次数
    payCheckCount = 0;    //回查次数计数

    // 页游信息
    bizno = 'yeyou';
    GAMES;  // 游戏数据
    GAMELIST:any[] = [];    // 按字母分组后的游戏数据
    gameList = [];  //配置的默认游戏 
    PYS: string = "abcdefghijklmnopqrstuvwxyz";   // 分组的字母
    excludeGame = ["05"]; //不包含的游戏, 00为页游，06为小游戏，05为手游，默认不显示05
    includeGame = ["00","06"];  // 包括的游戏，以及游戏姿态为 status = 1 时才显示
    gamePlayRecently = false; //最近在玩游戏
    serverPlayRecently = false; //最近在玩区服
    gamenameSel: string = '选择游戏';  // 游戏名称
    servernameSel: string = '';  // 选择的区服名称
    gameid; // 选择的游戏ID
    serverId;  // 选择的区服ID
    roleMy;   // 选择的角色
    roleid = '';  // 角色ID
    gamefenqus;  // 区服数据
    gamesMy;  // 最近玩的游戏
    isNeedRole =false; // 需要角色才能充值
    // fenquName;

    // 接口信息
    paySvrUrl: string = 'http://paysvr.niu.xunlei.com:8090/';  // 角色与扫码查询接口
    niuxWebsiteUrl: string = 'http://niu.xunlei.com/';  // 游戏区服接口
    gameUserInfo = 'http://websvr.niu.xunlei.com/userGameServerList.gameUserInfo';  // 最近玩的游戏和区服接口
    mPayUrl: string = 'https://agent-paycenter-ssl.xunlei.com/';   // 支付接口

    // 配置信息
    priceTags = [10,30,50,100,200,500,1000,5000,10000];  // 金额选项
    priceMy: number = 100;  // 默认选中金额
    isUserDefinedMoney = false; // 是否可自定义金额
    serverGroupNum = 5; //区服分组数
    extparam = '';  // 额外的参数
    NOWTIME: any = new Date().getTime();
    records: number = 10;

    priceLimit = 150000;
    count: number = 1;
    counts: number = 10;
    
    ieV;
    $mainTip;
    NiuxpayPage: HTMLElement = document.createElement('div');
    $username;
    $payDom;
    $gamesel;
    $payMoneys;
    $gamelist;
    $gamelistTab;
    $gameqflist;
    $serverSel;
    $roleSel;
    $roleList;
    $payways;
    $payway;
    $moneysSel;
    $pricePi;
    $payIn;
    $payOut;
    $qufuSearch;
    $payqrOff;
    $payQRImg;
    $NiuxPayPop1;
    $NiuxPayPop2;
    $NiuxPayPop3;
    $NiuxPayPop4;
    $NiuxPayPop5;
    $NiuxPayMask;
    $tab;
    $options;
    $optionOpen;
    $optionClose;

    
    // 构造初始化参数
    constructor(){
        if(typeof arguments[0] != undefined){
            let _config = arguments[0] || {};
            for(let obj in _config){
                if(obj in this){
                    this[obj] = _config[obj]
                }
            }
        }
        this.init();
    }
    // 重设默认参数
    _init(){
        if(typeof arguments[0] != undefined){
            let _config = arguments[0] || {};
            for(let obj in _config){
                if(obj in this){
                    this[obj] = _config[obj]
                }
            }
        }
    }
    // 初始化实例对象
    init() {
        // 添加样式
        this.createStyle();
        // 创建dom结构
        this.createDom();
        // 初始化jq对象
        this.initJqObj();
        // 发出游戏数据请求和更新DOM
        this.initGames();
    }
    createStyle(){
        let styleHtmlList = [];
        styleHtmlList.push('<style>');
        styleHtmlList.push(`#NiuxPayTs,dd,dl,form,h1,h2,h3,h4,h5,h6,p{margin:0}#NiuxPayTs,input,ol,ul{margin:0;padding:0}#NiuxPayTs,button,input,select,textarea{font:12px/1.5 微软雅黑, tahoma, arial, 宋体, sans-serif;outline:0}ol,ul{list-style:none}a{text-decoration:none;outline:0 none}img{border:0}table{border-collapse:collapse;border-spacing:0}button{cursor:pointer}i{font-style:normal}.fix:after{visibility:hidden;display:block;font-size:0;content:".";clear:both;height:0}.hide{display:none}.fc_red{color:#f3730e}.pay_pop_wp{background:#fff;border:#eaeaea solid 1px;box-shadow:0 0 5px #1a2c49;color:#999;width:720px;position:relative}.pay_pop_wp01{width:600px}.pay_pop_wp02{width:500px}.pay_pop_hd{background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat;height:34px;position:relative}.pay_pop_hd h3{font-size:16px;font-weight:400;color:#fff;line-height:34px;width:120px;text-align:center}.pay_pop_clo{background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -186px -42px;position:absolute;top:14px;right:18px;width:12px;height:12px;text-indent:-999em}.pay_pop_cont{background:#fff;color:#666;position:relative;padding:0 20px 10px}.img_wp img{display:block;width:100%;height:100%}.lastdiv{margin-right:0!important}.price_txt{margin:16px 0 7px;font-size:14px}.no_mgt{margin-top:0!important}.price_num{color:#fc8321}.price_num em{font-size:24px}.tip_grey{font-size:14px;line-height:1.5;color:#969696}.tip_grey a{color:#969696}.tip_grey a:hover{text-decoration:underline}.tip_orange{font-size:12px;color:#fc8321}a.tip_orange:hover{text-decoration:underline}.btn_orange{display:inline-block;width:164px;height:50px;line-height:50px;color:#fff;font-size:14px;text-align:center;background:#fc8321;transition:.2s}.btn_orange:hover{background:#fe9525}.ico_suc48x48,.ico_warn34x34,.ico_warn48x48{display:inline-block;vertical-align:middle;margin-right:12px;width:48px;height:48px;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat}.ico_warn48x48{background-position:0 -42px}.ico_warn34x34{background-position:-118px -108px;width:34px;height:34px}.ico_suc48x48{background-position:-59px -42px}.mark_tui{position:absolute;right:0;top:0;width:59px;height:59px;line-height:59;background:url(http://act.niu.xunlei.com/niuxpayts/img/ico_spr.png) no-repeat -150px -200px;overflow:hidden}.overhid{overflow:hidden}.txtcent{text-align:center}.mlr30{margin-left:30px;margin-right:30px}.mt28{margin-top:28px!important}.mt40{margin-top:40px!important}.mb10{margin-bottom:10px!important}.mb20{margin-bottom:20px!important}.mb30{margin-bottom:30px!important}.ml70{margin-left:70px}.plr45{padding:0 45px}.pl45{padding-left:45px}.pl90{padding-left:90px!important}.pt20{padding-top:20px}.pb40{padding-bottom:40px}.pt65{padding-top:65px!important}.pt190{padding-top:190px}.pb190{padding-bottom:190px}.top9{top:9px!important}.clr_orange2{color:#fc8321}.bb{border-bottom:1px solid #f5f5f5}.bt{border-top:1px solid #f5f5f5}.font14{font-size:14px}.bank_alipay,.bank_cft,.ico_bank{display:block;width:120px;height:33px;background:url(http://act.niu.xunlei.com/niuxpayts/img/spr_bank.png) no-repeat}.bank_ccb{background-position:0 -250px}.bank_icbc{background-position:0 -600px}.bank_cmb{background-position:0 -400px}.bank_boc{background-position:0 -150px}.bank_abc{background-position:0 -50px}.bank_ceb{background-position:0 -300px}.bank_cib{background-position:0 -350px}.bank_spdb{background-position:0 0}.bank_pab{background-position:0 -500px}.bank_cmbc{background-position:0 -450px}.bank_bosh{background-position:0 -200px}.bank_bjrcb{background-position:0 -100px}.bank_gdb{background-position:0 -550px}.bank_nbcb{background-position:0 -650px}.bank_bocm{background-position:0 -1050px}.bank_other{background-position:0 -700px}.bank_alipay{width:124px;height:38px;background-position:-10px -803px}.bank_cft{width:124px;height:38px;background-position:-10px -853px}.cnmobile{width:124px;height:38px;background-position:-10px -903px}.unicom{width:124px;height:38px;background-position:-10px -953px}.telecom{width:124px;height:38px;background-position:-10px -1003px}.fm_group{position:relative;margin:8px 0 0;padding-left:90px;font-size:14px}.fm_group .label{position:absolute;left:0;top:3px;line-height:2;font-size:14px;width:80px;color:#969696;text-align:right}.fm_group .txt_acc{display:block;line-height:30px}.fm_group .inp_txt{display:inline-block;width:283px;height:22px;padding:2px 10px;margin-right:10px;border:1px solid #e6e6e6;vertical-align:top}.fm_group .inp_txt_min{width:88px;clear:both}.fm_group .inp_txt_mid{width:240px}.fm_group .inp_txt_mid01{width:145px}.fm_group .inp_txt:focus{outline:0}.fm_group .authcode{display:inline-block;width:90px;height:40px;margin-right:8px;vertical-align:middle}.fm_group .authcode img{display:block;width:100%;height:100%;border:none}.fm_group .tex_tipmsg{margin:2px 0 -8px 0;font-size:12px;color:#d02525}.fm_group_min{text-align:left}.tip_sticky{display:block;width:512px;height:28px;line-height:28px;background-color:#fff6ef;font-size:12px;color:#969696;text-align:center}.tip_sticky .clr{color:#fc8321}.tab_area{overflow:hidden}.tab_area .tips{text-align:left;padding-top:6px}.tab_area .yzm{display:inline-block;vertical-align:top;width:82px;height:34px}.tab_area .yzm img{width:82px;height:34px}.tab_area .txt_change{display:inline-block;vertical-align:top;line-height:34px;padding-left:10px;color:#f15849;text-decoration:underline}.choicebox_wp{position:relative;zoom:1;padding-top:2px;overflow:hidden}.choicebox_wp .choicebox{position:relative;float:left;margin:0 8px 4px 0;min-width:120px;height:33px;background:#fff;border:1px solid #dcdcdc;text-align:center;cursor:pointer}.choicebox_wp .overhid{width:92%;padding-top:2px}.choicebox_wp .choicebox_txt{float:none;width:303px;height:33px}.choicebox_wp .choicebox_txtmini{margin-right:5px;width:148px;min-width:148px;height:33px}.choicebox_wp .choicebox_txt p,.choicebox_wp .choicebox_txtmini p{line-height:33px;font-size:14px}.choicebox.on{z-index:2}.choicebox.on:before{content:'';position:absolute;top:-1px;left:-1px;right:-1px;bottom:-1px;border:2px solid #fc8321}.choicebox:hover{border-color:#fc8321;z-index:1}.choicebox img{display:block;margin:0 auto}.choicebox .tit{display:block;height:33px;line-height:33px;font-size:14px;color:#323232}.choicebox .tip{display:block;color:#969696}.mark_nian{position:absolute;left:-9px;top:5px;width:66px;height:26px;background:#fc8321;color:#fff;line-height:26px;text-align:center}.mark_nian:after{content:"";position:absolute;left:0;bottom:-7px;width:0;height:0;border-width:4px;border-style:solid;border-color:#d66f1c #d66f1c transparent transparent}.mark_privilege{position:absolute;right:0;top:0;padding:0 6px;height:21px;line-height:21px;color:#fff;background:#fc8321}.mark_selected{display:none;position:absolute;right:-1px;bottom:-1px;overflow:hidden;width:20px;height:20px;line-height:30;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -152px -42px}.choicebox.on .mark_selected{display:block}.choicebox input.num{background:#fff;padding:6px 6px 7px;margin:0 auto;width:84px;height:20px;line-height:20px;text-align:center;outline:0;font-size:14px;border:none;color:#323232;font-weight:400}.choicebox.on input.num{position:relative;z-index:10;background:0 0}.choicebox .tip_orange{position:absolute;left:-2%;top:90px;width:104%}.choicebox_large .choicebox{width:124px;height:38px}.tab_list{position:relative;height:46px;border-bottom:1px solid #e3e3e3;background:#fff;font-size:0;white-space:nowrap}.tab_list a{display:inline-block;vertical-align:middle;position:relative;padding:0 9px;height:46px;line-height:46px;color:#a8a8a8;font-size:14px;cursor:pointer;transition:.2s}.tab_list a:after{content:'';position:absolute;right:0;top:50%;margin-top:-5px;vertical-align:middle;width:1px;height:11px;background:#e3e3e3}.tab_list a:hover{color:#fc8321}.tab_list a.on{color:#fc8321;border-left-color:#f5f5f5;border-right-color:#f5f5f5;border-bottom-color:#fff}.tab_list a.on:before{content:'';position:absolute;left:1%;bottom:-2px;width:98%;height:3px;background:#f3730e;z-index:1}.selectbox_wp:after{display:block;content:"";clear:both;height:0;position:relative}.select_box{position:relative;float:left;vertical-align:middle;margin-right:8px;height:35px}.selected{position:relative;width:120px;height:33px;border:1px solid #e6e6e6;background:#fff}.selected a{display:block;padding:0 43px 0 10px;height:100%;line-height:33px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;color:#4e4e4e}.select_box .option_open_btn{position:absolute;right:0;top:0;width:33px;height:33px;cursor:pointer}.select_box .option_open_btn i{position:absolute;left:50%;top:50%;margin:-3px 0 0 -6px;width:0;height:0;overflow:hidden;border-width:6px 6px 0;border-style:solid dashed;border-color:#a1a1a1 #fff}.select_box .option_open_btn .option_close{border-width:0 6px 6px}.select_box .option_open_btn:hover i{border-color:#fc8321 #fff!important}.option_wp{position:absolute;top:35px;left:0;width:512px;border:1px solid #e6e6e6;background:#fff;z-index:10;box-shadow:0 3px 5px rgba(0,0,0,.2)}.option_wp .option_txt{display:block;padding:0 13px;width:142px;height:28px;border-bottom:1px solid #e5e5e5;color:#969696;line-height:28px;transition:.2s}.option_wp .option_txt:hover{color:#fc8321}.select_box_2 .option_wp{left:-130px}.option_wp_2{position:absolute;top:35px;left:0;width:120px;max-height:370px;overflow:auto;border:1px solid #e6e6e6;background:#fff;box-shadow:0 3px 5px rgba(0,0,0,.2)}.option_wp_2 a{display:block;height:37px;line-height:37px;padding:0 10px;color:#6b6b6b;white-space:normal;overflow:hidden;text-overflow:ellipsis;transition:.2s}.option_wp_2 a:hover{background:#f3730e;color:#fff}.option_list{position:relative;padding:14px 0 10px;height:170px;overflow:auto}.option_group{position:relative;padding:0 12px 0 31px}.option_group ul{overflow:hidden}.option_group li{float:left;height:30px;line-height:30px;width:113px;margin-bottom:5px}.option_group li a{display:inline-block;max-width:99px;padding:0 6px;font-size:12px;color:#6b6b6b;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;transition:.2s}.option_group li a:hover{background:#f3730e;color:#fff}.option_group .keyword{position:absolute;top:5px;left:14px;color:#fc8321}.option_group_min{padding:0 12px}.option_group_min li{width:117px}.option_group_min li a{max-width:105px}.option_group_3col{padding:0 12px}.option_group_3col li{width:157px}.option_group_3col li a{max-width:140px}.search_mod{width:298px;margin:-14px auto 14px;padding:13px 57px;background:#e3e3e3}.ipt_search{position:relative;height:27px;background:#fff}.ipt_search input{display:block;border:none;height:17px;width:238px;padding:5px 30px}.ipt_search input:focus{outline:0}.ipt_search .ico_search{position:absolute;top:4px;left:6px;width:18px;height:18px;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -155px -69px}.ipt_wp{position:relative;margin-bottom:7px;line-height:30px}.ipt_wp .label{display:inline-block;margin-right:14px;height:30px;vertical-align:middle}.tick_box{display:inline-block;vertical-align:middle;margin-right:4px;width:16px;height:16px;border:1px solid #cbcbcb;border-radius:1px}.tick_box.on{background:url(http://act.niu.xunlei.com/niuxpayts/img/ico_spr.png) no-repeat -250px -125px}.tip_bg{padding:12px 20px;background:#fff6ef;text-align:center;font-size:14px}.paywaycont_wp{position:relative;padding:4px 0 0;background:#fff;clear:both;color:#969696;text-align:center;width:520px}.privilege_wp{padding-left:70px}.privilege_wp.fix_padl{padding-left:56px}.paywaycont_wp .btn_orange{margin-bottom:4px}.paywaycont_wp .lnk_addtic{color:#7e7e7e}.paywaycont_wp .lnk_addtic .ico_add{display:inline-block;vertical-align:-3px;width:15px;height:15px;margin-right:3px;background:url(http://act.niu.xunlei.com/niuxpayts/img/ico_spr.png) no-repeat -300px -100px}.paywaycont_wp .pay_btn_wp{padding:8px 22px 0 0}.qrcodepay_wp{position:relative;font-size:14px;line-height:1.5;text-align:left}.qrcodepay_wp .pic_qrcode{position:relative;width:160px;height:160px;margin-left:100px;background:#fff;border:1px solid #e5e5e5}.qrcodepay_wp .pic_qrcode img{display:block;width:100%}.qrcodepay_wp .pd{padding:20px 0 0 60px}.qrcodepay_wp .tip_payway{text-align:left;position:absolute;left:310px;bottom:60px}.qrcodepay_wp .pic_payway{margin-bottom:6px}.qrcodepay_wp .pic_payway .ico_alipay,.qrcodepay_wp .pic_payway .ico_wechat{display:inline-block;width:30px;height:30px;vertical-align:top;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat -36px -110px}.qrcodepay_wp .pic_payway .ico_wechat{background-position:-74px -110px}.qrcodepay_wp .ico_pointto{position:absolute;left:-40px;top:10px;width:29px;height:13px;background:url(http://act.niu.xunlei.com/niuxpayts/img/sp_pop.png) no-repeat 0 -118px}.pic_qrcode .qrcode_off{position:absolute;left:0;top:0;width:100%;height:100%}.pic_qrcode .qrcode_off .mask{position:absolute;left:0;top:0;width:100%;height:100%;background:#fff;-moz-opacity:.9;filter:alpha(opacity=90);opacity:.9}.pic_qrcode .qrcode_off .txt{position:relative;z-index:1;margin:68px auto 0;width:122px;height:28px;line-height:28px;text-align:center;color:#fff;background:#666;background:rgba(0,0,0,.6);border-radius:14px;cursor:default}.pay_rezult{padding:0 30px 30px}.pay_pop_wp .maintxt{margin-top:30px;font-size:16px;text-align:center}.pay_pop_wp .txt_rows li{line-height:30px;font-size:14px}.pay_pop_wp .p_btn_wp{width:100%;text-align:center}.pay_pop_wp .p_btn_wp .btn{text-indent:0;display:inline-block;vertical-align:top;margin:0 4px;padding:0 20px;min-width:80px;width:auto;height:40px;line-height:40px;color:#fff;font-size:12px;background:#ffb174;transition:.2s;letter-spacing:0}.pay_pop_wp .p_btn_wp .btn_orange{background:#fc8321;border-color:#fc8321;color:#fefefe;box-shadow:none;font-size:14px}.pay_pop_wp .p_btn_wp .btn_orange:hover{background:#fe9525}.pay_pop_wp .p_btn_wp .btn_blue{background:#4591ef;border-color:#4591ef;color:#fefefe;box-shadow:none;font-size:14px}.pay_pop_wp .p_btn_wp .btn_blue:hover{background:#5c9ff1}.pay_pop_wp .tips_min{padding-left:100px;font-size:14px}@-webkit-keyframes jump{0%,100%,12%,18%,24%,30%,6%{-webkit-transform:translateY(0)}15%,27%,3%{-webkit-transform:translateY(-5%)}21%,9%{-webkit-transform:translateY(5%)}}@keyframes jump{0%,100%,12%,18%,24%,30%,6%{transform:translateY(0)}15%,27%,3%{transform:translateY(-5%)}21%,9%{transform:translateY(5%)}}.zindex10{position:relative;z-index:10}.pay_pop_wp{position:fixed;top:50%;left:50%;margin-top:-254px;margin-left:-361px}.game_option_list{display:none}.game_option_list.on{display:block!important}#NiuxPayTs .fenqu_option_wp .option_group ul{display:none}#NiuxPayTs .fenqu_option_wp .option_group ul.on{display:block}#NiuxPayPop2{margin-left:-250px}#NiuxPayPop3,#NiuxPayPop4,#NiuxPayPop5{margin-left:-300px}#NiuxPayTs a:hover{text-decoration:none}`);
        styleHtmlList.push('</style>');
        $('head').append(styleHtmlList.join(''));
    }
    createDom(){
        this.NiuxpayPage = document.createElement('div');
        this.NiuxpayPage.setAttribute('id', 'NiuxPayTs');
        this.NiuxpayPage.style.cssText = 'display:none;';
        let PayPopDom = `<div class="niuxpay_mask" style="display:none;position:fixed;width:100%;height:100%;top:0;left:0;background:#000;opacity:.6;filter:alpha(opacity=60);z-index:10;"></div><div class=pay_pop_wp id=NiuxPayPop1 style="z-index:11"><div class=pay_pop_hd><h3>活动充值</h3><a href="#" title="关闭" class=pay_pop_clo>关闭</a></div><div class=pay_pop_cont><div class=pay_form><div class=fm_group><span class=label>充值账号：</span><span class=txt_acc>yangfei@163.com</span></div><div class="fm_group zindex10"><span class=label>充值信息：</span><div class=selectbox_wp><div class=select_box><div class=selected data-tab=1><a href="javascript:;" id=xlGameSel></a><span class=option_open_btn><i class=option_open></i><i class=option_close style=display:none></i></span></div><div class="option_wp game_option_wp" style=display:none><div class="tab_list tab_list_1"><a href="javascript:;" class=on data-gamestab=0>最近玩过的</a><a href="javascript:;" data-gamestab=1>ABCDE</a><a href="javascript:;" data-gamestab=2>FGHIJ</a><a href="javascript:;" data-gamestab=3>KLMNO</a><a href="javascript:;" data-gamestab=4>PQRST</a><a href="javascript:;" data-gamestab=5>UVWXYZ</a></div></div></div><div class="select_box select_box_2"><div class=selected data-tab=2><a href="javascript:;" id=xlGServerSel>选择区服</a><span class=option_open_btn><i class=option_open></i><i class=option_close style=display:none></i></span></div><div class="option_wp fenqu_option_wp" style=display:none><div class="tab_list tab_list_2"></div><div class=option_list><div class=search_mod><div class=ipt_search><i class=ico_search></i><input type=text id=NiuxPayQFs></div></div><div class="option_group option_group_min option_group_3col xl_fenqus_options"></div></div></div></div><div class=select_box><div class=selected data-tab=3><a href="javascript:;" id=xlGRoleSel>选择角色</a><span class=option_open_btn><i class=option_open></i><i class=option_close style=display:none></i></span></div><div class="option_wp option_wp_2 role_option_wp" style=display:none></div></div></div></div><div class=fm_group><span class=label>充值金额：</span><div class="choicebox_wp col5"><div class=choicebox_moneys></div><div class="choicebox choicebox-oth"><span class=tit style=display:none>其它</span><input class=num type=text placeholder="其他金额"><i class=mark_selected>选中</i></div></div></div><div class=fm_group><span class=label>支付方式：</span><div class="choicebox_wp col5" id=xlPayways><div class="choicebox on"><span class=tit>扫码支付</span><i class=mark_selected>选中</i></div><div class=choicebox><span class=tit>平台支付</span><i class=mark_selected>选中</i></div><div class=choicebox><span class=tit>网银支付</span><i class=mark_selected>选中</i></div><div class=choicebox><span class=tit>雷点支付</span><i class=mark_selected>选中</i></div></div><div class=paywaycont_wp id=xlPayway><div class=tab_area style=display:block><div class=qrcodepay_wp><div class=pic_qrcode><img src=http://act.niu.xunlei.com/niuxpayts/images/code.png alt="支付二维码"><div class=img_loading></div><div class=qrcode_off><div class=mask></div><div class=txt>请选择充值信息</div></div></div><div class=tip_payway><div class=pic_payway><span class=ico_alipay></span><span class=ico_wechat></span></div><p class=tip_grey>请使用微信或支付宝扫码支付</p><p class=tip_grey>同意并接受<a href=http://i.xunlei.com/tos.shtml target=_blank class=link>《迅雷支付服务协议》</a></p><span class=ico_pointto></span></div><p class="tip_grey pd">实付金额<strong class=fc_red><font class=payIn>100</font>元</strong>，您将获得<strong class=fc_red><font class=payOut>10000</font>元宝</strong></p></div></div><div class=tab_area style=display:none><div class="choicebox_wp choicebox_large"><div class="choicebox on" title="支付宝" data-paytype=E><span class="ico_bank bank_alipay"></span><i class=mark_selected>选中</i></div><div class=choicebox title="财付通" data-paytype=E2><span class="ico_bank bank_cft"></span><i class=mark_selected>选中</i></div></div><div class="pay_btn_wp mt20"><a href="javascript:;" class=btn_orange>立即充值</a><p class=tip_grey>实付金额<strong class=fc_red><font class=payIn>100</font>元</strong>，您将获得<strong class=fc_red><font class=payOut>10000</font>元宝</strong></p></div></div><div class=tab_area style=display:none><div class="choicebox_wp choicebox_mid"><div class="choicebox on" title="中国建设银行" data-bank=CCB><span class="ico_bank bank_ccb"></span><i class=mark_selected>选中</i></div><div class=choicebox title="中国工商银行" data-bank=ICBC><span class="ico_bank bank_icbc"></span><i class=mark_selected>选中</i></div><div class=choicebox title="招商银行" data-bank=CMB><span class="ico_bank bank_cmb"></span><i class=mark_selected>选中</i></div><div class=choicebox title="中国银行" data-bank=BOCB2C><span class="ico_bank bank_boc"></span><i class=mark_selected>选中</i></div><div class=choicebox title="中国农业银行" data-bank=ABC><span class="ico_bank bank_abc"></span><i class=mark_selected>选中</i></div><div class=choicebox title="中国光大银行" data-bank=CEBBANK><span class="ico_bank bank_ceb"></span><i class=mark_selected>选中</i></div><div class=choicebox title="兴业银行" data-bank=CIB><span class="ico_bank bank_cib"></span><i class=mark_selected>选中</i></div><div class=choicebox title="上海浦东发展银行" data-bank=SPDB><span class="ico_bank bank_spdb"></span><i class=mark_selected>选中</i></div><div class=choicebox title="平安银行" data-bank=SPABANK><span class="ico_bank bank_pab"></span><i class=mark_selected>选中</i></div><div class=choicebox title="中国民生银行" data-bank=CMBC><span class="ico_bank bank_cmbc"></span><i class=mark_selected>选中</i></div><div class=choicebox title="上海银行" data-bank=SHBANK><span class="ico_bank bank_bosh"></span><i class=mark_selected>选中</i></div><div class=choicebox title="北京农村商业银行" data-bank=BJRCB><span class="ico_bank bank_bjrcb"></span><i class=mark_selected>选中</i></div><div class=choicebox title="广东发展银行" data-bank=GDB><span class="ico_bank bank_gdb"></span><i class=mark_selected>选中</i></div><div class=choicebox title="宁波银行" data-bank=NBBANK><span class="ico_bank bank_nbcb"></span><i class=mark_selected>选中</i></div><div class=choicebox title="交通银行" data-bank=COMM><span class="ico_bank bank_bocm"></span><i class=mark_selected>选中</i></div><div class="choicebox choicebox_mini" title="其他银行" data-bank=""><span class="ico_bank bank_other"></span><i class=mark_selected>选中</i></div></div><div class="pay_btn_wp mt20"><a href="javascript:;" class=btn_orange>立即充值</a><p class=tip_grey>实付金额<strong class=fc_red><font class=payIn>100</font>元</strong>，您将获得<strong class=fc_red><font class=payOut>10000</font>元宝</strong></p></div></div><div class=tab_area style=display:none><div class=tip_sticky><p>可先使用手机充值卡、迅雷/天弘一卡通等先充<a href=https://pay.xunlei.com/ld.html class=clr target=_blank>雷点</a></p></div><div class="fm_group fm_group_min"><span class=label>账户余额：</span><p class=tips><font id=NiuxLD></font><span class=clr_gray>雷点</span>（1雷点=1元）</p></div><div class="fm_group fm_group_min"><span class=label>支付密码：</span><input type=password class="inp_txt inp_txt_mid" id=NiuxPayPs><p class=tex_tipmsg style="visibility:hidden;">支付密码不正确</p></div><div class="fm_group fm_group_min" id=NiuxPayVc><span class=label>验证码：</span><input type=password class="inp_txt inp_txt_mid01"><img class=yzm src=http://act.niu.xunlei.com/niuxpayts/images/yzm.jpg><a href="#" class=txt_change>换一个</a></div><div class="pay_btn_wp mt30"><a href="javascript:;" class=btn_orange>立即充值</a><p class=tip_grey>实付金额<strong class=fc_red><font class=payIn>100</font>元</strong>，您将获得<strong class=fc_red><font class=payOut>10000</font>元宝</strong></p></div></div></div></div></div></div></div><div class="pay_pop_wp pay_pop_wp02" id=NiuxPayPop2 style="display:none;z-index:12;"><div class=pay_pop_hd><h3>提示</h3><a href="#" title="关闭" class=pay_pop_clo>关闭</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class=maintxt>订单查询中······</p></div></div></div></div><div class="pay_pop_wp pay_pop_wp01" id=NiuxPayPop3 style="display:none;z-index:13;"><div class=pay_pop_hd><h3>提示</h3><a href="#" title="关闭" class=pay_pop_clo>关闭</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class="maintxt fc_red"><i class=ico_suc48x48></i>恭喜您充值成功</p><div class=tips_min><ol></ol></div></div></div><div class=p_btn_wp><a href="javascript:;" class="btn btn_orange">关闭</a></div></div></div><div class="pay_pop_wp pay_pop_wp01" id=NiuxPayPop4 style="display:none;z-index:14;"><div class=pay_pop_hd><h3>提示</h3><a href="#" title="关闭" class=pay_pop_clo>关闭</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class=maintxt><i class=ico_warn34x34></i>请在新打开的页面完成支付</p><ul><li>完成支付前请不要关闭此窗口<li>支付失败时，可拨打0755-61860414，迅雷客服竭诚为您服务！</ul></div></div><div class=p_btn_wp><a href="javascript:;" class="btn btn_orange" data-btn=200>已完成支付</a><a href="javascript:;" class=btn data-btn=1>支付失败，重试</a></div></div></div><div class="pay_pop_wp pay_pop_wp01" id=NiuxPayPop5 style="display:none;z-index:15;"><div class=pay_pop_hd><h3>提示</h3><a href="#" title="关闭" class=pay_pop_clo>关闭</a></div><div class=pay_pop_cont><div class=pay_rezult><div class=txt_rows><p class="maintxt fc_red"><i class=ico_warn48x48></i>请在新打开的页面完成支付</p>可能原因：<ol><li>1、您的支付过程没有完成。<li>2、银行/账户已扣款，但没有提示成功？网络传输问题可能造成延时，请不要担心，我们晶块会支付渠道对账后，恢复此服务，为您开通服务。<li>3、银行/账户没有扣款？建议您重新支付一次试试看如果仍无法成功，请联系相关银行/通信运营客服<li>4、如果那还不能帮到您，请拨打0755-61860414，迅雷客服竭诚为您服务。<li>支付失败时，可拨打0755-61860414，迅雷客服竭诚为您服务！</ol></div></div><div class=p_btn_wp><a href="javascript:;" class="btn btn_orange" data-btn=200>重新查询</a><a href="javascript:;" class=btn data-btn=1>取消</a></div></div></div> `;
        $(this.NiuxpayPage).html(PayPopDom);
        document.body.appendChild(this.NiuxpayPage);
    }
    initJqObj(){
        let _this = this;
        _this.$username = $('#NiuxPayTs .txt_acc');
        _this.$payDom = $('#NiuxPayTs');
        _this.$gamesel = $('#xlGameSel');  // 选择的游戏
        _this.$payMoneys = _this.$payDom.find('.choicebox_moneys');   // 充值金额列表
        _this.$gamelist = _this.$payDom.find('.game_option_wp');  // 游戏下拉列表
        _this.$gamelistTab = _this.$payDom.find('.tab_list_1');  // 游戏字母导航
        _this.$gameqflist = _this.$payDom.find('.fenqu_option_wp');  // 分区下拉列表
        _this.$serverSel = $('#xlGServerSel');  // 选择的区服
        _this.$roleSel = $('#xlGRoleSel');  // 选择的角色
        _this.$roleList = _this.$payDom.find('.role_option_wp');  // 角色列表
        _this.$payways = $('#xlPayways .choicebox');   // 支付方式切换
        _this.$payway = $('#xlPayway .choicebox');  // 支付方式下平台的切换
        _this.$pricePi = _this.$payDom.find('.choicebox-oth');  // 金额充值输入
        _this.$payqrOff = _this.$payDom.find('.qrcode_off');  // 二维码提示
        _this.$payQRImg = _this.$payDom.find('.pic_qrcode img'); 
        _this.$NiuxPayMask = _this.$payDom.find('.niuxpay_mask');

        _this.$optionOpen = _this.$payDom.find('.option_open');
        _this.$optionClose = _this.$payDom.find('.option_close');

        if(!_this.isUserDefinedMoney){
            // 不显示金额输入框
            _this.$pricePi.hide();
        }
        _this.$qufuSearch = $('#NiuxPayQFs');  // 区服搜索
        _this.$payIn = $('#xlPayway .payIn');  // 底下的金额显示
        _this.$payOut = $('#xlPayway .payOut'); // 相应的充值元宝

        // 浮层DOM
        _this.$NiuxPayPop1 = $('#NiuxPayPop1');
        _this.$NiuxPayPop2 = $('#NiuxPayPop2');
        _this.$mainTip = _this.$NiuxPayPop2.find('.maintxt');  // 浮层2提示文字
        _this.$NiuxPayPop3 = $('#NiuxPayPop3');
        _this.$NiuxPayPop4 = $('#NiuxPayPop4');
        _this.$NiuxPayPop5 = $('#NiuxPayPop5');

        // 获取 cookie
        _this.usernick = $.cookie('usernick') || $.cookie('usrname') || $.cookie('usernewno') || '没有登陆';
        _this.username = $.cookie('usrname') ||  $.cookie('usernewno') || '';
        _this.userid = $.cookie('userid') || '';
        _this.usernewno =  $.cookie("usernewno") || "";
        _this.sessionid = $.cookie('sessionid') || '';
        _this.mref = $.cookie('mref') || '';
        _this.niuxActNo = $.cookie('niuxActNo') || '';
        _this.niuxAdvNo = $.cookie('niuxAdvNo') || '';

        _this.unitPrice = _this.priceMy * 100;

        // 设置用户名和默认游戏名称
        _this.$username.text(this.usernick);
        _this.$gamesel.text(this.gamenameSel);

        $('#NiuxPayTs .select_box .selected').eq(2).hide(); // 默认不显示选择角色

        // ie8, ie9 不支持placeholder
        _this.ieV = parseInt($.browser.version);

        // ie8 indexOf
        if (!Array.prototype.indexOf)
        {
          Array.prototype.indexOf = function(elt)
          {
            var len = this.length >>> 0;
            var from = Number(arguments[1]) || 0;
            from = (from < 0)
                 ? Math.ceil(from)
                 : Math.floor(from);
            if (from < 0)
              from += len;
            for (; from < len; from++)
            {
              if (from in this &&
                  this[from] === elt)
                return from;
            }
            return -1;
          };
        }
    }
    initGames() {
        let _this = this;
        // 初始化游戏列表
        if(typeof(GAMES) == 'undefined' || GAMES == null){
            $.getScript('http://pay.niu.xunlei.com/js/config/baseConfig.js', function(){
                _this.initGames();
            });
            return;
        }
        _this.GAMES = GAMES || null;
        // 初始化充值金额列表选项
        let payMoneysTam = ``;
        let priceIdx = _this.priceTags.indexOf(_this.priceMy);
        for(let i = 0; i < _this.priceTags.length; i++){
            payMoneysTam += `<div class="choicebox`+(i == priceIdx? ` on` : ``) +`" data-money="`+ _this.priceTags[i] + `"><span class="tit">`+_this.priceTags[i] + `元</span> <i class="mark_selected">选中</i></div>`;
        }
        _this.$payMoneys.html(payMoneysTam);
        _this.$payIn.text(_this.priceMy);
        _this.$payOut.text(_this.priceMy*_this.payRatio);

        if(_this.ieV < 10){
            _this.$pricePi.find('input').val('其他金额');
        }

        
        // 默认可选的游戏
        if(_this.gameList.length > 0){
            let _GAMES = [];
            for(let g = 0; g < _this.gameList.length; g++){
                for(let gg in GAMES){
                    if(_this.gameList[g] == GAMES[gg].gameid){
                        _GAMES.push(GAMES[gg]);
                    }
                }
            }
            _this.gamesListUi(_GAMES);
        }else{
            _this.GAMESListUi(GAMES);
        }

        this.bindEvent();
    }
    // 默认可选的游戏
    gamesListUi(gl){
        let _this = this;
        _this.$gamelistTab.hide();
        let optionlist = `<div class="option_list game_option_list on" style="height:auto;"><div class="option_group"><ul>`;
        for(let j = 0; j < gl.length; j++){
            optionlist += `<li><a href="#" data-gamename="`+ gl[j].gamepath+ `">` + gl[j].name+ `</a></li>`;
        }
        optionlist += `</ul></div></div>`;
        _this.$gamelist.html(optionlist);
    }
    // 全部游戏
    GAMESListUi(gl){
        let _this = this;
        _this.$gamelistTab.show();
        // 按字母分组
        for(let i = 0; i < _this.PYS.length + 1; i++){
            _this.GAMELIST[i] = new Array();
        }
        let py;
        for(let g in gl){
            // console.log(gl[g]);
            if(gl[g].simpleName == undefined){
                continue;
            }
            if(gl[g].status == 2){
                continue;
            }
            if( _this.excludeGame.indexOf(gl[g].gameid.substr(0,2)) > -1){
                // 不包含的游戏
                continue;
            }
            if(_this.includeGame.indexOf(gl[g].gameid.substr(0,2)) < 0){
                // 包含的游戏
                continue;
            }
            
            py = gl[g].simpleName.toString().split("")[0].toLowerCase();
            if(_this.PYS.indexOf(py) > -1){
                _this.GAMELIST[_this.PYS.indexOf(py)].push(gl[g]);
            }
        }

    
        // 游戏列表 UI
        let optionlist = `<div class="option_list game_option_list on"></div><div class="option_list game_option_list">`;
        for(let j = 0; j < this.GAMELIST.length-1; j++){
            if(j == 5 || j == 10 || j == 15 || j == 20){
                optionlist += `</div><div class="option_list game_option_list">`;
            }
            if(this.GAMELIST[j].length != 0){
                optionlist += `<div class="option_group"><span class="keyword">` + this.PYS[j] + `-</span><ul>`;
                for(let k = 0; k < this.GAMELIST[j].length; k++){
                    optionlist += `<li><a href="#" data-gamename="`+ this.GAMELIST[j][k].gamepath+ `">` + this.GAMELIST[j][k].name+ `</a></li>`;
                }
                optionlist += `</ul></div>`;
            }
        }
        optionlist += `</div>`;
        _this.$gamelist.append(optionlist);
        _this.gamesMyListUI();
    }
    // 获取最近在玩的
    gamesMyListUI(){
        let _this = this;
        $.ajax({
            url: _this.gameUserInfo,
            type:'GET',
            dataType: 'jsonp',
            jsonp: 'rtnName',
            data:{
                records: _this.records,
                sessionid: _this.sessionid,
                username: _this.username
            },
            success: function(res){
                if(res.rtn == 0){
                    _this.gamesMy = res.data.data;
                    // console.log(_this.gamesMy);
                    let optionlist2 = `<div class="option_group"><ul>`;
                    for(let i = 0; i < res.data.data.length; i++){
                        optionlist2 += `<li><a href="#" data-gamename="`+ res.data.data[i].gamename+ `">` + res.data.data[i].name+ `</a></li>`;
                    }
                    optionlist2 += `</ul></div>`;
                    _this.$gamelist.find('.game_option_list').eq(0).html(optionlist2);
                }
            }
        });
    }
    selGame(gameno){
        let _this = this;
        _this.gameid = _this.GAMES[gameno].gameid;
        _this.isNeedRole = false;
        if(_this.GAMES[gameno].formType == 'usr'){
            _this.isNeedRole = true;
        }
        _this.servernameSel = '';
        _this.serverId = '';
        _this.$serverSel.text('选择区服');
        _this.$roleSel.text('选择角色');
        _this.roleMy = '';
        _this.$gamelist.toggle(); // 关闭游戏列表
        _this.$gameqflist.toggle(); // 展开区服列表
        // 获取游戏区服
        $.getScript(_this.niuxWebsiteUrl + 'gamedata/fenqu/'+ _this.gameid+'.json', function(){
            // console.log(gamefenqus);
            _this.gamefenqus = gamefenqus;
            if(_this.gamefenqus.length < 1) return;
            if(_this.gamefenqus[0].fenQuNum > _this.gamefenqus[_this.gamefenqus.length -1].fenQuNum){
                _this.gamefenqus = _this.gamefenqus.reverse();
            }
            _this.gamefenqusUI(_this.gamefenqus);

            
        });
        // 最近玩的区服
        $.ajax({
            url: _this.gameUserInfo,
            type:'GET',
            dataType: 'jsonp',
            jsonp: 'rtnName',
            data:{
                gameId: _this.gameid,
                records: _this.records,
                sessionid: _this.sessionid,
                username: _this.username
            },
            success: function(res){
                if(res.rtn == 0){
                    _this.gamesMy = res.data.data;
                    let optionlist2 = ``;
                    for(let i = 0; i < _this.gamesMy.length; i++){
                        optionlist2 += `<li><a href="#" data-serverid="` + _this.gamesMy[i].serverid+`">` + _this.gamesMy[i].fenqunickname + `</a></li>`;
                    }
                    _this.$gameqflist.find('.xl_fenqus_options ul').eq(0).html(optionlist2);
                }
            }
        });
    }
    // 选择区服
    selServer(serverId){
        let _this = this;
        // _this.fenquName = _this.gamefenqus.filter((v,i) => (v.serverId == serverId)); 
        // _this.fenquName = _.filter(_this.gamefenqus, (v) => (v.serverId == serverId)); // 筛选出选择的区服信息
        // console.log(_this.fenquName[0]);

        _this.$gameqflist.toggle(); // 关闭区服列表

        _this.$roleSel.text('选择角色');
        _this.roleMy = '';
        //需要查询角色
        var rtnName = 'getGameRole' + (new Date()).getTime() + Math.floor(Math.random()*1000);
        $.getScript(_this.paySvrUrl+'gamepaycenter/getRole.role?gameid='+_this.gameid+'&serverid='+_this.serverId+'&username='+_this.username+'&rtnName='+rtnName, function(){
            _this.rtnName = window[rtnName];
            // console.log(_this.rtnName);
            if(_this.rtnName.code == '00'){
                let rolesHTML = ``;
                for(let m in _this.rtnName.roleMap){
                    rolesHTML += `<a href="#" data-roleid="`+ m + `">` + decodeURIComponent(_this.rtnName.roleMap[m]) + `</a>`;
                }
                _this.$roleList.html(rolesHTML);
                $('#NiuxPayTs .select_box .selected').eq(2).show();
            }else{
                _this.$roleList.html('');
                _this.roleMy = '0';
                _this.$roleSel.text('无角色');
                if(_this.isPayQR){
                    _this.makeQrcode();
                }
            }
            
        });

    }
    gamefenqusUI(gf){
        // 区服的分组
        // console.log(gamefenqus);
        let fenquUITam;
        let _this = this;
        // }
        // console.log(gf);
        let fqlistTam = `<a href="javascript:;" class="on" data-qufu="0">最近玩过的</a>`;
        let fenquTemplate = `<ul class="on"></ul>`;
        if(gf.length >= 100){
            let fqC = 0;
            let fqL = [];
            let fqM;
            if(gf.length > 500){
                fqM = 100;
            }else{
                fqM = Math.floor(gf.length / _this.serverGroupNum);
            }
            let fqI = 0;
            for(let i in gf){
                if(!gf[i]){
                    continue;
                }
                if(!fqL[fqC]){
                    fqL[fqC] = [];
                }
                fqL[fqC].push(gf[i]);
                fqI ++;
                if(fqI > fqM && fqL.length < _this.serverGroupNum){
                    fqC += 1;
                    fqI = 0;
                }
            }
            for(let j = 0; j < fqL.length; j++){
                fqlistTam += `<a href="javascript:;"  data-qufu="`+ Number(j+1) +`">`+fqL[j][0].fenQuNum+`-`+fqL[j][fqL[j].length -1].fenQuNum+`</a>`;
                fenquTemplate += `<ul>`;
                for(let k = 0; k < fqL[j].length; k++){
                    fenquTemplate += `<li><a href="#" data-serverid="` + fqL[j][k].serverId+`">` + fqL[j][k].fenQuName+ `</a></li>`;
                }
                fenquTemplate += `</ul>`;
            }
        }
        else{
            fqlistTam += `<a href="javascript:;"  data-qufu="1">`+gf[0].fenQuNum+`-`+gf[gf.length -1].fenQuNum+`</a>`;
            fenquTemplate += `<ul>`;
            for(let k = 0; k < gf.length; k++){
                fenquTemplate += `<li><a href="#" data-serverid="` + gf[k].serverId+`">` + gf[k].fenQuName+ `</a></li>`;
            }
            fenquTemplate += `</ul>`;
        }
        _this.$gameqflist.find('.tab_list_2').html(fqlistTam);

        fenquTemplate += `<ul></ul>`;  // 前一个 ul 组给最近玩的，最后那个 ul 组是给搜索的啊 
        _this.$gameqflist.find('.xl_fenqus_options').html(fenquTemplate);

    }

    //获取玩家选择的支付数据
    getPayData(){
        let _this = this;
        var payObj = {
            tradeid: _this.tradeid,
            userid: _this.userid,
            version: _this.version,
            sessionid: _this.sessionid,
            opid: _this.opid,
            activeid: _this.activeid,
            bizno:_this.bizno,
            num: _this.priceMy,
            rmb: _this.unitPrice,
            mref: _this.mref,
            referfrom: _this.niuxAdvNo
        };
        return payObj;
    }
    checkPayInfo(){
        let _this = this;
        if(_this.sessionid == undefined || _this.sessionid == ''){
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            _this.$mainTip.text('请先登陆');
            return false;
        }
        if(_this.gameid == undefined || _this.gameid == ''){
            if(_this.isPayQR){
                return false;
            }
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            _this.$mainTip.text('请选择游戏');
            return false;
        }
        if(_this.serverId == ''){
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            _this.$mainTip.text('请选择区服');
            return false;
        }
        if(_this.isNeedRole){
            _this.$NiuxPayPop1.hide();
            _this.$NiuxPayPop2.show();
            if(_this.roleMy == '0'){
                _this.$mainTip.text('暂无角色');
                return false;
            }
            _this.$mainTip.text('请选择角色');
            return false;
        }
        return true;
    }
    // 扫码支付，生成支付二维码
    makeQrcode(){
        let _this =this;
        
        if(!_this.checkPayInfo()){
            return;
        }

        let url = _this.mPayUrl+"vippay/qrcode";
        _this.originalOrderid = _this.makeOriginalOrderid();
        let ext2 = 'userId='+_this.userid+'&userName='+_this.username+'&gameId='+_this.gameid+'&gameName='+_this.gamenameSel+'&unitPrice='+_this.unitPrice+'&serverId='+_this.serverId+'&servername='+_this.servernameSel+'&roleId='+this.roleid+'&roleName='+_this.roleMy+'&originalOrderid='+_this.originalOrderid+'&bankno='+_this.bank+'&niuxActNo='+_this.niuxActNo+'&extparam='+_this.extparam+'&niuxAdvNo='+_this.niuxAdvNo;
        let ext3 = '{"mAid":"101"}';

        let params = _this.getPayData();
        params = $.extend(params,{
            ext2: ext2,
            ext3: ext3
        });
        $.ajax({
            url: url,
            type:'GET',
            dataType: 'jsonp',
            jsonp:'callback',
            data:params,
            success: function(res){
                if(res.ret == 200){
                    let qrcodeImg = new Image();
                    qrcodeImg.onload = function () {
                        _this.$payQRImg.attr('src', this.src);
                        _this.$payqrOff.hide();
                    };
                    qrcodeImg.src = res.data["qrcode"];
                    _this.setCheckOrderInter(3000);
                }else{
                    _this.$payqrOff.show();
                    _this.$NiuxPayPop1.hide();
                    _this.$NiuxPayPop2.show();
                    _this.$mainTip.text(res.msg);
                    // $('#NiuxPayPop1 .qrcode_off .text').text(res.msg);
                    // _this.$payqrOff.find('.qrcode_off .txt').txt(res.msg);
                }
            }
        });
    }
    //设置订单回查定时器
    setCheckOrderInter(interTime){
        let _this = this;
        clearInterval(_this.checkPayOrderInter);
        _this.checkPayOrderInter = setInterval(function(){
            //console.log(interTime);
            _this.checkOrderStatus();
            _this.payOrderCheckTimes++;

            if(_this.payOrderCheckTimes >= _this.checkLimitTimes){
                _this.clearCheckOrderAction();
            }
        }, interTime ? interTime : 5000);
    }
    //发起回查订单
    checkOrderStatus(){
        /* success */
        //return;
        let _this = this;
        if(_this.payOrderCheckTimes%5 == 0){
            var interTime = 3000 + (_this.payOrderCheckTimes*500);
            _this.setCheckOrderInter(interTime);
        }else{
            var queryUrl = _this.paySvrUrl+"gamepaycenter/pay?action=queryOriginOrder&rtnName=chkOrderRtn&originalOrderid=" + _this.originalOrderid+ '&rd='+Math.random();
            $.getScript(queryUrl, function(rs){
                var code = parseInt(chkOrderRtn.code);
                if(code == 0){
                    _this.clearCheckOrderAction();
                    _this.orderId = chkOrderRtn.orderid;
                    _this.niupayway = '扫码支付';
                    _this.paySuccess();
                }else if(code == 2){
                    //充值查询
                    // _this.$NiuxPayPop2.show();
                }else if(code == 1){
                    //支付成功,但游戏点划拨暂未成功,请稍后查询
                    if(_this.payCheckCount >= 2){
                        //充值错误，连续获得改状态3次
                        // _this.showPayFail();
                        _this.clearCheckOrderAction();
                    }
                    _this.payCheckCount++;
                    // _this.showPayFail();
                }else{
                    //充值错误
                    // _this.showPayFail();
                    _this.clearCheckOrderAction();
                }
            });
        }
    }
    paySuccess(){
        let _this = this;
        _this.$NiuxPayPop1.hide();
        _this.$NiuxPayPop2.hide();
        _this.$NiuxPayPop4.hide();
        _this.$NiuxPayPop5.hide();
        _this.$NiuxPayPop3.show();

        _this.username = $.cookie('usrname');

        let tipTam = `<li>充值游戏：`+ _this.gamenameSel+`</li><li>实付金额：`+ Number(_this.priceMy*_this.payRatio) +`元宝/`+ _this.priceMy +`元`;
        tipTam += `<li>充值帐号：`+_this.username+`</li><li>充值区服：`+ _this.servernameSel +`</li><li>支付方式：`+_this.niupayway+`</li>`;
        _this.$NiuxPayPop3.find('ol').html(tipTam);
        _this.getPayOkTipsHandler();
    }
    getPayOkTipsHandler(){

    }
    showPayFail(){
        let _this = this;
        _this.$NiuxPayPop1.hide();
        _this.$NiuxPayPop2.hide();
        _this.$NiuxPayPop3.hide();
        _this.$NiuxPayPop4.hide();
        _this.$NiuxPayPop5.show();
        _this.getPayFailTipsHandler();
    }
    getPayFailTipsHandler(){

    }
    //清除订单回查定时器
    clearCheckOrderAction(){
        let _this = this;
        clearInterval(_this.checkPayOrderInter);
        _this.payOrderCheckTimes = 0;
    }
    //制造原始订单号
    makeOriginalOrderid(){
        let _this = this;
        var userNewNo = _this.usernewno;

        userNewNo = (userNewNo && userNewNo.length <= 10) ? userNewNo : _this.getRandomString(10);

        var originalOrderid = Math.floor(_this.NOWTIME/1000) + userNewNo + _this.getRandomString(5);
        while(originalOrderid.length < 25){
            originalOrderid = originalOrderid+_this.getRandomString(1);
        }
        return originalOrderid;
    }
    getRandomString(len){
        let _len = len || 25;
        let $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'; // 默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1
        let maxPos = $chars.length;
        let pwd = '';
        for(var i = 0; i < _len; i++){
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    }
    // 支付宝，财富通，银行，雷点
    netpayGo(){
        let _this = this;
        if(!_this.checkPayInfo()){
            return;
        }

        let url = _this.mPayUrl+'vippay/buy';
        _this.originalOrderid = _this.makeOriginalOrderid();
        let ext2 = 'userId='+_this.userid+'&userName='+_this.username+'&gameId='+_this.gameid+'&gameName='+_this.gamenameSel+'&unitPrice='+_this.unitPrice+'&serverId='+_this.serverId+'&servername='+_this.servernameSel+'&roleId='+this.roleid+'&roleName='+_this.roleMy+'&goodstimes=1&niuxActNo='+_this.niuxActNo+'&extparam='+_this.extparam+'&niuxAdvNo='+_this.niuxAdvNo;
        let _ext3 = {
            name: _this.gamenameSel,
            des: _this.gamenameSel+'|'+_this.servernameSel+'|'+_this.unitPrice,
            mAid: 101,
            mLogin: 1
        };
        let ext3 = encodeURIComponent(JSON.stringify(_ext3));

        let params = _this.getPayData();
        params = $.extend(params, {
            ext2: ext2,
            ext3: ext3,
            aidFrom: '',
            paytype: _this.paytype,
            bankNo: _this.bank,
            fgUrl: window.location.href
        });

        
        if(_this.paytype == 'A1'){
            let _other1= ''; // 密码
            let verify_code = '';  // 验证码
            _other1 = hex_md5($.trim($('#NiuxPayPs').val()));
            verify_code = $.trim($('#NiuxPayVc input').val());
            params.activeid = '2005002';
            params = $.extend(params, {
                other1: _other1,
                VERIFY_CODE: verify_code
            });
        }

        $.ajax({
            url: url,
            type:'GET',
            dataType: 'jsonp',
            jsonp:'callback',
            data:params,
            success: function(res){
                if(res.ret == 200){
                    if(res.data.code == 200){
                        _this.orderId = res.data.orderId;
                        _this.$NiuxPayPop1.hide();
                        _this.$NiuxPayPop4.show();
                        $('.tex_tipmsg').css('visibility','hidden');
                        window.open(res.data.url);
                    }else if(res.data.code == 201){
                            $('#NiuxPayPs').val('')
                            $('#NiuxPayVc input').val('')
                            _this.paySuccess();
                    }
                    else{
                        _this.checkVerCode();
                        $('.tex_tipmsg').text(res.msg).css('visibility','visible');
                    }
                }
                else if(res.ret == -1){
                    // 需要验证码
                    if(_this.paytype != 'A1'){
                        _this.$NiuxPayPop1.hide();
                        _this.$NiuxPayPop2.show();
                        _this.$mainTip.text(res.msg);
                        return;
                    }
                    
                    $('.tex_tipmsg').text(res.msg).css('visibility','visible');
                    _this.checkVerCode();
                }
            }
        });
    }
    checkVerCode(){
        let _this = this;
        let timeStamp = new Date();
        let verfCodeUrl = 'https://captcha-ssl.xunlei.com/image?t=MDA&cachetime='+timeStamp;
        $('#NiuxPayVc img').attr('src', verfCodeUrl);
    }
    checkNetpayStatus(){
        let _this = this;
        _this.$NiuxPayPop1.hide();
        _this.$NiuxPayPop2.show();
        _this.$mainTip.text('订单查询中······');
        let url = _this.mPayUrl+'vippay/issucc';
        $.ajax({
            url: url,
            type:'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            data:{
                version: _this.version,
                orderId: _this.orderId,
                opid: _this.opid,
                userid: _this.userid,
                sessionid: _this.sessionid
            },
            success: function(res){
                if(res.ret == 200){
                    if(res.data.fpaysucc == 1){
                        _this.paySuccess();
                    }else{
                        _this.showPayFail();
                    }
                }
            }
        });
    }
    checkLeiDian(){
        let _this = this;
        let url = _this.mPayUrl+'minfo/ldQuery';
        $.ajax({
            url: url,
            type:'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            success: function(res){
                if(res.ret == 200){
                       $('#NiuxLD').text(res.data);
                }
            }
        });
    }
    show(){
        this.$payDom.show();
        this.$NiuxPayMask.show();
    }
    hide(){
        this.$payDom.hide();
        this.$NiuxPayMask.hide();
    }
    bindEvent() {
        let _this = this;
        // 显示支付页
        _this.show();
        // 点击关闭
        _this.$NiuxPayPop1.find('.pay_pop_clo').bind('click', function (e) {
            e.preventDefault();
            _this.clearCheckOrderAction();
            _this.hide();
        });
        // 提示浮层关闭
        _this.$NiuxPayPop2.find('.pay_pop_clo').bind('click', function (e) {
            e.preventDefault();
            _this.$NiuxPayPop2.hide();
            _this.$NiuxPayPop1.show();
        });
        // tab选项切换
        $('#NiuxPayTs .select_box .selected').bind('click', function (e) {
            e.stopPropagation();
            let t = $(e.currentTarget).data('tab')
            _this.$tab= t;
            if(_this.$tab == 1){
                _this.$gameqflist.hide();
                _this.$roleList.hide();
            }else if(_this.$tab == 2){
                if(_this.gameid == undefined || _this.gameid == '') return;
                _this.$gamelist.hide();
                _this.$roleList.hide();
            }else{
                _this.$gamelist.hide();
                _this.$gameqflist.hide();
            }
            $(this).parent('.select_box').find('.option_wp').toggle();
            $(this).find('.option_open').toggle();
            $(this).find('.option_close').toggle();
            for(let tt = 0; tt < 3; tt++){
                if(tt == t-1) continue;
                _this.$optionClose.eq(tt).hide();
                _this.$optionOpen.eq(tt).show();
            }
        });

        // 游戏列表选择
        // 选择游戏
        _this.$gamelist.bind('click', function(e){
            e.preventDefault();
            let $e = $(e.target);
            // 点击了游戏tab
            if($e.data('gamestab') != undefined){
                // console.log($e.data('gamestab'));
                let idx = $e.data('gamestab');
                $e.addClass('on').siblings().removeClass('on');
                _this.$gamelist.find('.game_option_list').eq(idx).addClass('on').siblings().removeClass('on');
            }
            // 点击了游戏 id
            if($e.data('gamename') != undefined){
                _this.gameid = $e.data('gamename');
                _this.gamenameSel = $e.text();
                _this.$gamesel.text(_this.gamenameSel);
                _this.selGame(_this.gameid);
                _this.$gamelist.parent().find('.option_open').toggle();
                _this.$gamelist.parent().find('.option_close').toggle();
                _this.$gameqflist.parent().find('.option_open').toggle();
                _this.$gameqflist.parent().find('.option_close').toggle();
            }
            
        });

        // 区服列表选择
        _this.$gameqflist.bind('click', function(e){
            e.preventDefault();
            let $e = $(e.target);
            // 点击了区服 tab
            if($e.data('qufu') != undefined){
                let idx = $e.data('qufu');
                $e.addClass('on').siblings().removeClass('on');
                _this.$gameqflist.find('.xl_fenqus_options ul').eq(idx).addClass('on').siblings().removeClass('on');
            }
            // 点击了分区
            if($e.data('serverid') != undefined){
                _this.serverId = $e.data('serverid');
                _this.servernameSel = $e.text();
                _this.$serverSel.text(_this.servernameSel);
                _this.selServer(_this.serverId);
                _this.$gameqflist.parent().find('.option_open').toggle();
                _this.$gameqflist.parent().find('.option_close').toggle();
            }
        });

        // 区服搜索
        // IE8 要用 textchange 事件
        _this.$qufuSearch.bind('textchange', function(e){
            let _serverName = e.target.value;
            let _servers = [];
            for(let i = 0; i < _this.gamefenqus.length; i++ ){
                if(_this.gamefenqus[i].fenQuName.indexOf(_serverName) > -1){
                    _servers.push(_this.gamefenqus[i]);
                }
            }
            let _serversHTML = ``;
            for(let j = 0; j < _servers.length; j++){
                _serversHTML += `<li><a href="#" data-serverid="` + _servers[j].serverId+`">` + _servers[j].fenQuName+ `</a></li>`;
            }
            _this.$gameqflist.find('.xl_fenqus_options ul').eq(-1).html(_serversHTML).addClass('on').siblings().removeClass('on');
        });

        // 角色的选择
        _this.$roleList.bind('click', function(e){
            e.preventDefault();
            let $e = $(e.target);
            if($e.data('roleid') != undefined){
                let id = $e.data('roleid');
                _this.roleid = id;
                _this.$roleSel.text($e.text());
                _this.roleMy = $e.text();
                _this.$roleList.toggle();
                _this.isNeedRole = false;
                if(_this.isPayQR){
                    _this.makeQrcode();
                }
            }
        }); 

        // 支付方式切换
        _this.$payways.bind('click', function() {
            var tab_index = $(this).index();
            if(tab_index == 3){
                _this.isPayQR = false;
                _this.paytype = 'A1';
                _this.clearCheckOrderAction();
                _this.niupayway = '雷点支付';
                _this.checkVerCode();
                _this.checkLeiDian();
            }
            else if(tab_index == 1){
                _this.isPayQR = false;
                _this.paytype = _this.netpaytype;
                _this.clearCheckOrderAction();
            }
            else if(tab_index == 2){
                _this.isPayQR = false;
                _this.paytype = 'B';
                _this.clearCheckOrderAction();
            }
            else{
                _this.isPayQR = true;
                _this.makeQrcode();
            }
            $(this).addClass('on').siblings().removeClass('on');
            $('#xlPayway .tab_area').eq(tab_index).show().siblings().hide();
        });

        // 支付方式下平台的切换
        _this.$payway.bind('click', function(e){
            let $e = $(e.currentTarget);
            $(this).addClass('on').siblings().removeClass('on');
            // 银行
            if($e.data('bank') != undefined){
                _this.bank = $e.data('bank');
                _this.niupayway = $e.attr('title');
            }
            // 支付宝或财付通
            if($e.data('paytype') != undefined){
                _this.netpaytype = $e.data('paytype');
                _this.paytype = _this.netpaytype;
                _this.niupayway = $e.attr('title');
            }
        });
        _this.$NiuxPayPop1.find('.btn_orange').bind('click', function(e){
            e.preventDefault();
            _this.netpayGo();
        });
        // 选择充值额度
        _this.$payMoneys.find('.choicebox').bind('click', function(){
            _this.count += 1;
            if(_this.count > _this.counts){
                _this.count = 1;
                _this.$NiuxPayPop1.hide();
                _this.$NiuxPayPop2.show();
                _this.$mainTip.text('订单请求过于频繁，请稍后再试');
                return;
            }
            $(this).addClass('on').siblings().removeClass('on');
            _this.$pricePi.removeClass('on');
            _this.priceMy = $(this).data('money');
            _this.unitPrice = _this.priceMy * 100;
            // console.log(_this.priceMy);
            _this.$payIn.text(_this.priceMy);
            _this.$payOut.text(_this.priceMy*_this.payRatio);
            _this.$payqrOff.show();
            _this.$pricePi.find('input').val('');
            if(_this.ieV < 10){
                _this.$pricePi.find('input').val('其他金额');
            }
            if(_this.isPayQR){
                _this.makeQrcode();
            }
            
        });

        // 输入充值额度
        // IE8 要用 textchange 事件

        _this.$pricePi.find('input').bind('textchange',function(e){
            let _val = $(this).val().replace(/\D/ig,"");
            _val = parseInt(_val) || 1;
            if(_val > _this.priceLimit){
                _val =  _this.priceLimit - 1;
            }
            $(this).val(_val);
            _this.priceMy = _val || 1;
            _this.unitPrice = _this.priceMy * 100;
            _this.$payIn.text(_this.priceMy);
            _this.$payOut.text(_this.priceMy*_this.payRatio);
            _this.$payqrOff.show();
        }).bind('blur', function(e){
            if(e.target.value != ''){
                $(this).val(_this.priceMy + '元');
            }
            if(e.target.value == '' && _this.ieV < 10){
                $(this).val('其他金额');
            }
            if(_this.isPayQR && _this.priceMy != 0){
                _this.makeQrcode();
            }
        }).bind('focus', function(e){
            _this.$payMoneys.find('.choicebox').removeClass('on');
            _this.$pricePi.addClass('on');
            $(this).val('');
            _this.priceMy = 0;
            _this.unitPrice = _this.priceMy * 100;
            _this.$payIn.text(_this.priceMy);
            _this.$payOut.text(_this.priceMy*_this.payRatio);
        });

        // 成功页面点击关闭
        _this.$NiuxPayPop3.find('a').bind('click', function(e){
            e.preventDefault();
            _this.$NiuxPayPop1.show();
            _this.$NiuxPayPop3.hide();
        });
        // 支付查询
        _this.$NiuxPayPop4.find('a').bind('click', function(e){
            e.preventDefault();
            let $e = $(e.target),
                btnCode = $e.data('btn');
            if($e.data('btn') != undefined){
                if(btnCode == 200){
                    _this.checkNetpayStatus();
                }else{
                    _this.$NiuxPayPop1.show();
                    _this.$NiuxPayPop4.hide();
                }
            }
        });
        // 支付失败查询
        _this.$NiuxPayPop5.find('a').bind('click', function(e){
            e.preventDefault();
            let $e = $(e.target),
                btnCode = $e.data('btn');
            if($e.data('btn') != undefined){
                if(btnCode == 200){
                    _this.checkNetpayStatus();
                }else{
                    _this.$NiuxPayPop1.show();
                    _this.$NiuxPayPop5.hide();
                }
            }
        });
        // 更换验证码
        $('#NiuxPayVc a').bind('click', function(e){
            e.preventDefault();
            _this.checkVerCode();
        });
    }
}
